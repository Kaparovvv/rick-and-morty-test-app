import 'package:dio/dio.dart';

import 'package:rick_and_morty_app/app/data/api_client/api_client.dart';
import 'package:rick_and_morty_app/app/domain/app_config.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';

@module
abstract class InjectableModule {
  // ignore: invalid_annotation_target
  // @preResolve
  // Future<SharedPreferences> get sharedPreferences =>
  //     SharedPreferences.getInstance();

  @lazySingleton
  Dio dio(AppConfig appConfig) => ApiClient.configureDio(appConfig);

  // @lazySingleton
  // Connectivity get connectivity => Connectivity();
}
