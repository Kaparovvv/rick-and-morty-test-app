import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:rick_and_morty_app/di/injection.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  asExtension: true,
)
void configureDependencies(String env) => getIt.init(environment: env);
