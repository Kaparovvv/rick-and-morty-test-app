// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i5;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:rick_and_morty_app/app/data/main_app_config.dart' as _i4;
import 'package:rick_and_morty_app/app/domain/app_config.dart' as _i3;
import 'package:rick_and_morty_app/di/injectable_module.dart' as _i18;
import 'package:rick_and_morty_app/features/character/data/datasources/remote/character_api_service.dart'
    as _i6;
import 'package:rick_and_morty_app/features/character/data/repositories/character_repo_impl.dart'
    as _i11;
import 'package:rick_and_morty_app/features/character/domain/repositories/character.dart'
    as _i10;
import 'package:rick_and_morty_app/features/character/domain/usecases/fetch_characters.dart'
    as _i14;
import 'package:rick_and_morty_app/features/character/domain/usecases/filter_characters.dart'
    as _i15;
import 'package:rick_and_morty_app/features/character/domain/usecases/usecases.dart'
    as _i17;
import 'package:rick_and_morty_app/features/character/presentation/blocs/character/character_bloc.dart'
    as _i16;
import 'package:rick_and_morty_app/features/episode/data/datasources/remote/episode_api_service.dart'
    as _i7;
import 'package:rick_and_morty_app/features/episode/data/repositories/episode_repo_impl.dart'
    as _i9;
import 'package:rick_and_morty_app/features/episode/domain/repositories/episode.dart'
    as _i8;
import 'package:rick_and_morty_app/features/episode/domain/usecases/fetch_episodes.dart'
    as _i12;
import 'package:rick_and_morty_app/features/episode/domain/usecases/filter_episode.dart'
    as _i13;

const String _test = 'test';
const String _dev = 'dev';
const String _prod = 'prod';

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final injectableModule = _$InjectableModule();
    gh.singleton<_i3.AppConfig>(
      () => _i4.TestAppConfig(),
      registerFor: {_test},
    );
    gh.singleton<_i3.AppConfig>(
      () => _i4.DevAppConfig(),
      registerFor: {_dev},
    );
    gh.singleton<_i3.AppConfig>(
      () => _i4.ProdAppConfig(),
      registerFor: {_prod},
    );
    gh.lazySingleton<_i5.Dio>(() => injectableModule.dio(gh<_i3.AppConfig>()));
    gh.singleton<_i6.CharacterApiService>(
        () => _i6.CharacterApiService(gh<_i5.Dio>()));
    gh.singleton<_i7.EpisodeApiService>(
        () => _i7.EpisodeApiService(gh<_i5.Dio>()));
    gh.singleton<_i8.EpisodeRepository>(
        () => _i9.EpisodeRepoImpl(gh<_i7.EpisodeApiService>()));
    gh.singleton<_i10.CharacterRepository>(
        () => _i11.CharacterRepoImpl(gh<_i6.CharacterApiService>()));
    gh.singleton<_i12.FetchEpisodesUseCase>(
        () => _i12.FetchEpisodesUseCase(gh<_i8.EpisodeRepository>()));
    gh.singleton<_i13.FilterEpisodesUseCase>(
        () => _i13.FilterEpisodesUseCase(gh<_i8.EpisodeRepository>()));
    gh.singleton<_i14.FetchCharactersUseCase>(
        () => _i14.FetchCharactersUseCase(gh<_i10.CharacterRepository>()));
    gh.singleton<_i15.FilterCharactersUseCase>(
        () => _i15.FilterCharactersUseCase(gh<_i10.CharacterRepository>()));
    gh.factory<_i16.CharacterBloc>(() => _i16.CharacterBloc(
          gh<_i17.FetchCharactersUseCase>(),
          gh<_i17.FilterCharactersUseCase>(),
        ));
    return this;
  }
}

class _$InjectableModule extends _i18.InjectableModule {}
