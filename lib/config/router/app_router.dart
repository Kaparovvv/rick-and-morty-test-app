import 'package:rick_and_morty_app/core/packages/packages.dart';

import 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: '/',
          page: NavBarRoute.page,
          children: [
            AutoRoute(path: 'characters', page: CharactersRoute.page),
            AutoRoute(path: 'episodes', page: EpisodesRoute.page),
          ],
        ),
        AutoRoute(path: '/character', page: CharacterRoute.page),
        AutoRoute(path: '/episode', page: EpisodeRoute.page),
      ];
}
