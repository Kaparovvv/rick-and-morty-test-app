// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i6;
import 'package:flutter/material.dart' as _i7;
import 'package:rick_and_morty_app/app/presentation/navbar/navbar_page.dart'
    as _i5;
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart'
    as _i8;
import 'package:rick_and_morty_app/features/character/presentation/pages/character.dart'
    as _i1;
import 'package:rick_and_morty_app/features/character/presentation/pages/characters.dart'
    as _i2;
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart'
    as _i9;
import 'package:rick_and_morty_app/features/episode/presentation/pages/episode.dart'
    as _i3;
import 'package:rick_and_morty_app/features/episode/presentation/pages/episodes.dart'
    as _i4;

abstract class $AppRouter extends _i6.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i6.PageFactory> pagesMap = {
    CharacterRoute.name: (routeData) {
      final args = routeData.argsAs<CharacterRouteArgs>();
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.CharacterPage(
          key: args.key,
          character: args.character,
        ),
      );
    },
    CharactersRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.CharactersPage(),
      );
    },
    EpisodeRoute.name: (routeData) {
      final args = routeData.argsAs<EpisodeRouteArgs>();
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i3.EpisodePage(
          key: args.key,
          episode: args.episode,
        ),
      );
    },
    EpisodesRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i4.EpisodesPage(),
      );
    },
    NavBarRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i5.NavBarPage(),
      );
    },
  };
}

/// generated route for
/// [_i1.CharacterPage]
class CharacterRoute extends _i6.PageRouteInfo<CharacterRouteArgs> {
  CharacterRoute({
    _i7.Key? key,
    required _i8.CharacterEntity character,
    List<_i6.PageRouteInfo>? children,
  }) : super(
          CharacterRoute.name,
          args: CharacterRouteArgs(
            key: key,
            character: character,
          ),
          initialChildren: children,
        );

  static const String name = 'CharacterRoute';

  static const _i6.PageInfo<CharacterRouteArgs> page =
      _i6.PageInfo<CharacterRouteArgs>(name);
}

class CharacterRouteArgs {
  const CharacterRouteArgs({
    this.key,
    required this.character,
  });

  final _i7.Key? key;

  final _i8.CharacterEntity character;

  @override
  String toString() {
    return 'CharacterRouteArgs{key: $key, character: $character}';
  }
}

/// generated route for
/// [_i2.CharactersPage]
class CharactersRoute extends _i6.PageRouteInfo<void> {
  const CharactersRoute({List<_i6.PageRouteInfo>? children})
      : super(
          CharactersRoute.name,
          initialChildren: children,
        );

  static const String name = 'CharactersRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}

/// generated route for
/// [_i3.EpisodePage]
class EpisodeRoute extends _i6.PageRouteInfo<EpisodeRouteArgs> {
  EpisodeRoute({
    _i7.Key? key,
    required _i9.EpisodeEntity episode,
    List<_i6.PageRouteInfo>? children,
  }) : super(
          EpisodeRoute.name,
          args: EpisodeRouteArgs(
            key: key,
            episode: episode,
          ),
          initialChildren: children,
        );

  static const String name = 'EpisodeRoute';

  static const _i6.PageInfo<EpisodeRouteArgs> page =
      _i6.PageInfo<EpisodeRouteArgs>(name);
}

class EpisodeRouteArgs {
  const EpisodeRouteArgs({
    this.key,
    required this.episode,
  });

  final _i7.Key? key;

  final _i9.EpisodeEntity episode;

  @override
  String toString() {
    return 'EpisodeRouteArgs{key: $key, episode: $episode}';
  }
}

/// generated route for
/// [_i4.EpisodesPage]
class EpisodesRoute extends _i6.PageRouteInfo<void> {
  const EpisodesRoute({List<_i6.PageRouteInfo>? children})
      : super(
          EpisodesRoute.name,
          initialChildren: children,
        );

  static const String name = 'EpisodesRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}

/// generated route for
/// [_i5.NavBarPage]
class NavBarRoute extends _i6.PageRouteInfo<void> {
  const NavBarRoute({List<_i6.PageRouteInfo>? children})
      : super(
          NavBarRoute.name,
          initialChildren: children,
        );

  static const String name = 'NavBarRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}
