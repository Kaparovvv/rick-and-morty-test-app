part of 'theme.dart';

ThemeData kLight() {
  return ThemeData(
    useMaterial3: true,
    fontFamily: FontFamily.intern,
    bottomNavigationBarTheme: _bottomNavigationBarTheme,
    elevatedButtonTheme: _elevatedButtonTheme,
    dropdownMenuTheme: _dropdownMenuTheme,
    snackBarTheme: _snackBarTheme,
  );
}

final _snackBarTheme = SnackBarThemeData(
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
);

const _bottomNavigationBarTheme = BottomNavigationBarThemeData(
  showUnselectedLabels: true,
  type: BottomNavigationBarType.fixed,
);

final _elevatedButtonTheme = ElevatedButtonThemeData(
  style: ElevatedButton.styleFrom(
    backgroundColor: _lightColorScheme.primary,
    disabledBackgroundColor: _lightColorScheme.secondary,
    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
    minimumSize: const Size(double.infinity, 56),
    maximumSize: const Size(double.infinity, 56),
    elevation: 0,
    textStyle: const TextStyle(color: AppColors.kWhite),
  ),
);

final _dropdownMenuTheme = DropdownMenuThemeData(
  inputDecorationTheme: InputDecorationTheme(
    fillColor: AppColors.kBgTextField,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(16),
      borderSide: BorderSide(color: AppColors.kGrey500),
    ),
  ),
);
