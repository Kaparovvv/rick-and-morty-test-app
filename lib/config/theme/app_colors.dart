part of 'theme.dart';

class AppColors {
  static const kPrimary = Color(0xFFD02C24);
  static const kOnPrimary = Color(0xFFFFC043);
  static const kBgPrimary = Color(0xffFEF9F5);
  static const kBgTextField = Color(0xFFf4f7f9);
  static const kLightBlue = Color(0xFFF5F7F9);
  static const kBlack = Color(0xFF000000);
  static const kWhite = Color(0xFFFFFFFF);
  static Color kGrey300 = Colors.grey.shade300;
  static Color kGrey500 = Colors.grey.shade500;
  static Color kGrey600 = Colors.grey.shade600;
  static Color kGrey700 = Colors.grey.shade700;
  static const kRed = Colors.red;
  static const kGreen = Colors.green;
  static const kBrown = Colors.brown;
  static const kOrange = Colors.orange;
  static const kFieldFill = Color(0xFF76B9FE);
}
