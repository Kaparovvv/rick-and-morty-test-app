part of 'theme.dart';

const _lightColorScheme = ColorScheme(
  brightness: Brightness.light,
  primary: AppColors.kPrimary,
  onPrimary: AppColors.kOnPrimary,
  secondary: Color(0xFFFF8A73),
  onSecondary: AppColors.kLightBlue,
  onError: Color(0x66D02C24),
  surface: AppColors.kWhite,
  onSurface: AppColors.kBlack,
  outline: AppColors.kBgTextField,
  error: AppColors.kRed,
);
