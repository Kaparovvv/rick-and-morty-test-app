import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/core/gen/fonts.gen.dart';

part 'color_scheme.dart';
part 'text_theme.dart';
part 'light_theme.dart';
part 'app_colors.dart';
