part of 'theme.dart';

const textTheme = TextTheme(
  titleLarge: TextStyle(
    fontSize: 24,
    height: 1.21,
    fontWeight: FontWeight.w400,
  ),
  titleMedium: TextStyle(
    fontSize: 20,
    height: 1.3,
    fontWeight: FontWeight.w400,
  ),
  titleSmall: TextStyle(
    fontSize: 18,
    height: 1.3,
    fontWeight: FontWeight.w400,
  ),
  labelLarge: TextStyle(
    fontSize: 16,
    height: 1.44,
    fontWeight: FontWeight.w400,
  ),
  labelMedium: TextStyle(
    fontSize: 14,
    height: 1.44,
    fontWeight: FontWeight.w400,
  ),
  labelSmall: TextStyle(
    fontSize: 12,
    height: 1.44,
    letterSpacing: 0,
    fontWeight: FontWeight.w400,
  ),
);
