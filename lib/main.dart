import 'package:rick_and_morty_app/app/presentation/main_app_builder.dart';
import 'package:rick_and_morty_app/app/presentation/main_app_runner.dart';

const appEnv = 'APPENV';

void main() async {
  const env = String.fromEnvironment(appEnv, defaultValue: 'prod');
  const runner = MainAppRunner(env);
  final builder = MainAppBuilder();
  runner.run(builder);
}
