import 'package:rick_and_morty_app/app/data/data_state/data_state.dart';
import 'package:rick_and_morty_app/app/data/usecase/usecase.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/characters.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';
import 'package:rick_and_morty_app/features/character/domain/repositories/character.dart';

@singleton
class FetchCharactersUseCase
    implements UseCase<DataState<CharactersEntity>, int> {
  FetchCharactersUseCase(this._repository);

  final CharacterRepository _repository;

  @override
  Future<DataState<CharactersEntity>> call({required int params}) async {
    return await _repository.fetchCharacters(params);
  }
}
