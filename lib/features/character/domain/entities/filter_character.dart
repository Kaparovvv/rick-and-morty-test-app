import 'package:rick_and_morty_app/core/packages/packages.dart';

class FilterCharacterEntity extends Equatable {
  const FilterCharacterEntity({
    required this.name,
    required this.species,
    required this.type,
    required this.gender,
    required this.status,
  });

  final String name, species, type, gender, status;

  @override
  List<Object?> get props => [name, species, type, gender, status];
}
