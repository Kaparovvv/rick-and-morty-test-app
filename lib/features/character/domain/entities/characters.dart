import 'package:rick_and_morty_app/app/domain/entities/entities.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/character.dart';

class CharactersEntity extends Equatable {
  final InfoEntity info;
  final List<CharacterEntity> results;

  const CharactersEntity({
    required this.info,
    required this.results,
  });

  @override
  List<Object?> get props => [info, results];
}
