import 'package:rick_and_morty_app/core/packages/packages.dart';

class CharacterLocationEntity extends Equatable {
  final String name, url;

  const CharacterLocationEntity({
    required this.name,
    required this.url,
  });

  @override
  List<Object> get props => [name, url];
}
