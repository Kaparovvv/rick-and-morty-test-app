import 'package:rick_and_morty_app/core/packages/packages.dart';

class CharacterOriginEntity extends Equatable {
  final String name, url;

  const CharacterOriginEntity({
    required this.name,
    required this.url,
  });

  @override
  List<Object> get props => [name, url];
}
