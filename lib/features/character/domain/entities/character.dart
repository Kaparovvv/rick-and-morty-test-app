import 'package:rick_and_morty_app/core/enums/enums.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/character_location.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/character_origin.dart';

class CharacterEntity extends Equatable {
  final int id;
  final String name, species, type, image;
  final CharacterStatus status;
  final CharacterGender gender;
  final CharacterOriginEntity origin;
  final CharacterLocationEntity location;
  final List<String> episode;
  final String url;
  final DateTime created;

  const CharacterEntity({
    required this.id,
    required this.name,
    required this.status,
    required this.species,
    required this.type,
    required this.gender,
    required this.origin,
    required this.location,
    required this.image,
    required this.episode,
    required this.url,
    required this.created,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        status,
        species,
        type,
        gender,
        origin,
        location,
        image,
        episode,
        url,
        created,
      ];
}
