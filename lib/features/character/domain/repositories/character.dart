import 'package:rick_and_morty_app/app/data/data_state/data_state.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';

abstract class CharacterRepository {
  Future<DataState<CharactersEntity>> fetchCharacters(int page);
  Future<DataState<CharactersEntity>> filterCharacters(
    FilterCharacterEntity queries,
  );
}
