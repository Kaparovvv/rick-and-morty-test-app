import 'package:formz/formz.dart';

class CharacterFilterForm with FormzMixin {
  CharacterFilterForm({
    this.name = const CharacterNameForm.pure(),
    this.species = const CharacterSpeciesForm.pure(),
    this.type = const CharacterTypeForm.pure(),
    this.gender = const CharacterGenderForm.pure(),
    this.characteStatus = const CharacterStatusForm.pure(),
  });

  final CharacterNameForm name;
  final CharacterSpeciesForm species;
  final CharacterTypeForm type;
  final CharacterGenderForm gender;
  final CharacterStatusForm characteStatus;

  @override
  List<FormzInput> get inputs => [name, species, type, gender, characteStatus];
}

enum CharacterNameValidationError { empty }

class CharacterNameForm
    extends FormzInput<String, CharacterNameValidationError> {
  const CharacterNameForm.pure() : super.pure('');
  const CharacterNameForm.dirty([super.value = '']) : super.dirty();

  @override
  CharacterNameValidationError? validator(String value) {
    if (value.isEmpty) return CharacterNameValidationError.empty;
    return null;
  }
}

enum CharacterSpeciesValidationError { empty }

class CharacterSpeciesForm
    extends FormzInput<String, CharacterSpeciesValidationError> {
  const CharacterSpeciesForm.pure() : super.pure('');
  const CharacterSpeciesForm.dirty([super.value = '']) : super.dirty();

  @override
  CharacterSpeciesValidationError? validator(String value) {
    if (value.isEmpty) return CharacterSpeciesValidationError.empty;
    return null;
  }
}

enum CharacterTypeValidationError { empty }

class CharacterTypeForm
    extends FormzInput<String, CharacterTypeValidationError> {
  const CharacterTypeForm.pure() : super.pure('');
  const CharacterTypeForm.dirty([super.value = '']) : super.dirty();

  @override
  CharacterTypeValidationError? validator(String value) {
    if (value.isEmpty) return CharacterTypeValidationError.empty;
    return null;
  }
}

enum CharacterGenderValidationError { empty }

class CharacterGenderForm
    extends FormzInput<String, CharacterGenderValidationError> {
  const CharacterGenderForm.pure() : super.pure('');
  const CharacterGenderForm.dirty([super.value = '']) : super.dirty();

  @override
  CharacterGenderValidationError? validator(String value) {
    if (value.isEmpty) return CharacterGenderValidationError.empty;
    return null;
  }
}

enum CharacteStatusValidationError { empty }

class CharacterStatusForm
    extends FormzInput<String, CharacteStatusValidationError> {
  const CharacterStatusForm.pure() : super.pure('');
  const CharacterStatusForm.dirty([super.value = '']) : super.dirty();

  @override
  CharacteStatusValidationError? validator(String value) {
    if (value.isEmpty) return CharacteStatusValidationError.empty;
    return null;
  }
}
