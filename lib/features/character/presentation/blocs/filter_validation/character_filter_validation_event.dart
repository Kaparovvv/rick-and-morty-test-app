part of 'character_filter_validation_bloc.dart';

sealed class CharacterFilterValidationEvent extends Equatable {
  const CharacterFilterValidationEvent();

  @override
  List<Object> get props => [];
}

final class CharacterNameChangedEvent
    extends CharacterFilterValidationEvent {
  const CharacterNameChangedEvent(this.name);

  final String name;

  @override
  List<Object> get props => [name];
}

final class CharacterSpeciesChangedEvent
    extends CharacterFilterValidationEvent {
  const CharacterSpeciesChangedEvent(this.species);

  final String species;

  @override
  List<Object> get props => [species];
}

final class CharacterTypeChangedEvent
    extends CharacterFilterValidationEvent {
  const CharacterTypeChangedEvent(this.type);

  final String type;

  @override
  List<Object> get props => [type];
}

final class CharacterGenderChangedEvent
    extends CharacterFilterValidationEvent {
  const CharacterGenderChangedEvent(this.gender);

  final String gender;

  @override
  List<Object> get props => [gender];
}

final class CharacterStatusChangedEvent
    extends CharacterFilterValidationEvent {
  const CharacterStatusChangedEvent(this.status);

  final String status;

  @override
  List<Object> get props => [status];
}
