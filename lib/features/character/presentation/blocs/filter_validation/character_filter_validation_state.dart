part of 'character_filter_validation_bloc.dart';

final class CharacterFilterValidationState extends Equatable with FormzMixin {
  const CharacterFilterValidationState({
    this.status = FormzSubmissionStatus.initial,
    this.name = const CharacterNameForm.pure(),
    this.species = const CharacterSpeciesForm.pure(),
    this.type = const CharacterTypeForm.pure(),
    this.gender = const CharacterGenderForm.pure(),
    this.characteStatus = const CharacterStatusForm.pure(),
    this.isValidForm = false,
  });

  final FormzSubmissionStatus status;
  final CharacterNameForm name;
  final CharacterSpeciesForm species;
  final CharacterTypeForm type;
  final CharacterGenderForm gender;
  final CharacterStatusForm characteStatus;
  final bool isValidForm;

  CharacterFilterValidationState copyWith({
    FormzSubmissionStatus? status,
    CharacterNameForm? name,
    CharacterSpeciesForm? species,
    CharacterTypeForm? type,
    CharacterGenderForm? gender,
    CharacterStatusForm? characteStatus,
    bool? isValidForm,
  }) {
    return CharacterFilterValidationState(
      status: status ?? this.status,
      name: name ?? this.name,
      species: species ?? this.species,
      type: type ?? this.type,
      gender: gender ?? this.gender,
      characteStatus: characteStatus ?? this.characteStatus,
      isValidForm: isValidForm ?? this.isValidForm,
    );
  }

  @override
  List<Object> get props =>
      [status, name, species, type, gender, characteStatus, isValid];

  @override
  List<FormzInput> get inputs => [name, species, type, gender, characteStatus];
}
