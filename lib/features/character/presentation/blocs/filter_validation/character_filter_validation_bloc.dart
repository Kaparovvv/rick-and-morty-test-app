import 'package:formz/formz.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/presentation/forms/filter_forms.dart';

part 'character_filter_validation_event.dart';
part 'character_filter_validation_state.dart';

class CharacterFilterValidationBloc extends Bloc<CharacterFilterValidationEvent,
    CharacterFilterValidationState> {
  CharacterFilterValidationBloc()
      : super(const CharacterFilterValidationState()) {
    on<CharacterNameChangedEvent>(_onNameChanged);
    on<CharacterSpeciesChangedEvent>(_onSpeciesChanged);
    on<CharacterTypeChangedEvent>(_onTypeChanged);
    on<CharacterGenderChangedEvent>(_onGenderChanged);
    on<CharacterStatusChangedEvent>(_onStatusChanged);
  }
  void _onNameChanged(
    CharacterNameChangedEvent event,
    Emitter<CharacterFilterValidationState> emit,
  ) {
    final name = CharacterNameForm.dirty(event.name);
    emit(
      state.copyWith(
        name: name,
        isValidForm: CharacterFilterForm().isValid,
      ),
    );
  }

  void _onSpeciesChanged(
    CharacterSpeciesChangedEvent event,
    Emitter<CharacterFilterValidationState> emit,
  ) {
    final species = CharacterSpeciesForm.dirty(event.species);
    emit(
      state.copyWith(
        species: species,
        isValidForm: CharacterFilterForm().isValid,
      ),
    );
  }

  void _onTypeChanged(
    CharacterTypeChangedEvent event,
    Emitter<CharacterFilterValidationState> emit,
  ) {
    final type = CharacterTypeForm.dirty(event.type);
    emit(
      state.copyWith(
        type: type,
        isValidForm: CharacterFilterForm().isValid,
      ),
    );
  }

  void _onGenderChanged(
    CharacterGenderChangedEvent event,
    Emitter<CharacterFilterValidationState> emit,
  ) {
    final gender = CharacterGenderForm.dirty(event.gender);
    emit(
      state.copyWith(
        gender: gender,
        isValidForm: CharacterFilterForm().isValid,
      ),
    );
  }

  void _onStatusChanged(
    CharacterStatusChangedEvent event,
    Emitter<CharacterFilterValidationState> emit,
  ) {
    final status = CharacterStatusForm.dirty(event.status);
    emit(
      state.copyWith(
        characteStatus: status,
        isValidForm: CharacterFilterForm().isValid,
      ),
    );
  }
}
