import 'dart:async';

import 'package:dio/dio.dart';
import 'package:rick_and_morty_app/app/data/data_state/data_state.dart';
import 'package:rick_and_morty_app/app/data/error_handler/error_handler.dart';
import 'package:rick_and_morty_app/core/droppables/droppables.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';
import 'package:rick_and_morty_app/features/character/domain/usecases/usecases.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';

part 'character_event.dart';
part 'character_state.dart';

@injectable
class CharacterBloc extends Bloc<CharacterEvent, CharacterState> {
  CharacterBloc(
    this._fetchCharactersUseCase,
    this._filterCharactersUseCase,
  ) : super(const CharacterState()) {
    on<FetchCharactersEvent>(
      _onFetch,
      transformer: throttleDroppable(const Duration(microseconds: 100)),
    );
    on<RefreshCharactersEvent>(
      _onRefresh,
      transformer: throttleDroppable(const Duration(microseconds: 100)),
    );
    on<FilterCharactersEvent>(_onFilter);
    on<SearchCharacterEvent>(
      _onSearch,
      transformer: debounceDroppable(const Duration(milliseconds: 200)),
    );
    on<ResetFilterOrSearchCharacterEvent>(_onResetFilterOrSearch);
  }

  final FetchCharactersUseCase _fetchCharactersUseCase;
  final FilterCharactersUseCase _filterCharactersUseCase;
  int _page = 1;
  List<CharacterEntity> _characters = [];

  Future<void> _onFetch(
    FetchCharactersEvent event,
    Emitter<CharacterState> emit,
  ) async {
    if (state.hasReachedMax) return;
    if (state.status == CharacterBlocStatus.initial) {
      final result = await _fetchCharactersUseCase.call(params: _page);
      _page++;

      if (result is DataSuccess && result.data != null) {
        _characters = result.data!.results;
        return emit(state.copyWith(
          status: CharacterBlocStatus.success,
          characters: result.data?.results,
          hasReachedMax: false,
          hasFiltered: false,
        ));
      }
      if (result is DataError) {
        final handle = ErrorHandler.handle(result.error);
        final message = handle.failure.message;
        return emit(state.copyWith(
          status: CharacterBlocStatus.failure,
          errorMessage: message,
        ));
      }
    }
    final result =
        await _fetchCharactersUseCase.call(params: event.page ?? _page);
    if (result is DataSuccess && result.data != null) {
      _page++;
      final characters = result.data!.results;
      _characters = characters;
      emit(state.copyWith(
        status: CharacterBlocStatus.success,
        characters: List.of(state.characters)..addAll(characters),
        hasReachedMax: result.data!.info.next == null,
        hasFiltered: false,
        hasSearched: false,
      ));
    }
    if (result is DataError) {
      final handle = ErrorHandler.handle(result.error);
      final message = handle.failure.message;
      return emit(state.copyWith(
        status: CharacterBlocStatus.failure,
        errorMessage: message,
      ));
    }
  }

  Future<void> _onRefresh(
    RefreshCharactersEvent event,
    Emitter<CharacterState> emit,
  ) async {
    try {
      _page = 1;
      final result = await _fetchCharactersUseCase.call(params: _page);
      _page++;

      if (result is DataSuccess && result.data != null) {
        _characters = result.data!.results;
        return emit(state.copyWith(
          status: CharacterBlocStatus.success,
          characters: result.data?.results,
          hasReachedMax: false,
          hasFiltered: false,
        ));
      }
      if (result is DataError) {
        final handle = ErrorHandler.handle(result.error);
        final message = handle.failure.message;
        return emit(state.copyWith(
          status: CharacterBlocStatus.failure,
          errorMessage: message,
        ));
      }
    } finally {
      event.completer?.complete();
    }
  }

  Future<void> _onFilter(
    FilterCharactersEvent event,
    Emitter<CharacterState> emit,
  ) async {
    emit(state.copyWith(status: CharacterBlocStatus.initial));
    final result =
        await _filterCharactersUseCase.call(params: event.filterQueries);

    if (result is DataSuccess) {
      return emit(state.copyWith(
        status: CharacterBlocStatus.success,
        characters: result.data?.results,
        hasFiltered: true,
        hasReachedMax: false,
      ));
    }
    if (result is DataError) {
      if (result.error is DioException) {
        final error = result.error as DioException;
        if (error.response?.statusCode == 404) {
          emit(state.copyWith(
            status: CharacterBlocStatus.noCharacter,
            hasFiltered: true,
          ));
          add(const ResetFilterOrSearchCharacterEvent());
        }
      }
      final handle = ErrorHandler.handle(result.error);
      final message = handle.failure.message;
      return emit(state.copyWith(
        status: CharacterBlocStatus.failure,
        errorMessage: message,
      ));
    }
  }

  Future<void> _onSearch(
    SearchCharacterEvent event,
    Emitter<CharacterState> emit,
  ) async {
    emit(state.copyWith(status: CharacterBlocStatus.initial));
    final result = _characters
        .where((item) => item.name.toLowerCase().contains(
              event.query.toLowerCase(),
            ))
        .toList();

    emit(state.copyWith(
      status: result.isNotEmpty
          ? CharacterBlocStatus.success
          : CharacterBlocStatus.noCharacter,
      characters: result.isNotEmpty ? result : [],
      hasReachedMax: false,
      hasFiltered: false,
      hasSearched: true,
      hasReset: event.query.isNotEmpty,
    ));
  }

  void _onResetFilterOrSearch(
    ResetFilterOrSearchCharacterEvent event,
    Emitter<CharacterState> emit,
  ) {
    emit(state.copyWith(
      status: CharacterBlocStatus.success,
      characters: _characters,
      hasReachedMax: false,
      hasFiltered: false,
      hasSearched: false,
      hasReset: false,
    ));
  }
}
