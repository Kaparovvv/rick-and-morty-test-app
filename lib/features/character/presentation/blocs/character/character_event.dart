part of 'character_bloc.dart';

sealed class CharacterEvent extends Equatable {
  const CharacterEvent();

  @override
  List<Object?> get props => [];
}

final class FetchCharactersEvent extends CharacterEvent {
  const FetchCharactersEvent({this.page});

  final int? page;

  @override
  List<Object?> get props => [page];
}

final class RefreshCharactersEvent extends CharacterEvent {
  const RefreshCharactersEvent({this.completer});

  final Completer? completer;

  @override
  List<Object?> get props => [completer];
}

final class FilterCharactersEvent extends CharacterEvent {
  const FilterCharactersEvent({required this.filterQueries});

  final FilterCharacterEntity filterQueries;

  @override
  List<Object> get props => [filterQueries];
}

final class SearchCharacterEvent extends CharacterEvent {
  const SearchCharacterEvent({required this.query});

  final String query;

  @override
  List<Object?> get props => [query];
}

final class ResetFilterOrSearchCharacterEvent extends CharacterEvent {
  const ResetFilterOrSearchCharacterEvent();
}
