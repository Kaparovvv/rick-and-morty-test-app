part of 'character_bloc.dart';

enum CharacterBlocStatus { initial, success, failure, noCharacter }

final class CharacterState extends Equatable {
  const CharacterState({
    this.status = CharacterBlocStatus.initial,
    this.characters = const <CharacterEntity>[],
    this.hasReachedMax = false,
    this.errorMessage = '',
    this.hasFiltered = false,
    this.hasSearched = false,
    this.hasReset = false,
  });
  final CharacterBlocStatus status;
  final List<CharacterEntity> characters;
  final bool hasReachedMax;
  final String errorMessage;
  final bool hasFiltered;
  final bool hasSearched;
  final bool hasReset;

  CharacterState copyWith({
    CharacterBlocStatus? status,
    List<CharacterEntity>? characters,
    bool? hasReachedMax,
    String? errorMessage,
    bool? hasFiltered,
    bool? hasSearched,
    bool? hasReset,
  }) {
    return CharacterState(
      status: status ?? this.status,
      characters: characters ?? this.characters,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      errorMessage: errorMessage ?? this.errorMessage,
      hasFiltered: hasFiltered ?? this.hasFiltered,
      hasSearched: hasSearched ?? this.hasSearched,
      hasReset: hasReset ?? this.hasReset,
    );
  }

  @override
  String toString() {
    return '''CharacterState { status: $status, hasReachedMax: $hasReachedMax, character: ${characters.length}, errorMessage: $errorMessage, hasFilteredCharacters: $hasFiltered, hasSearched: $hasSearched, hasReset: $hasReset}''';
  }

  @override
  List<Object> get props => [
        status,
        characters,
        hasReachedMax,
        errorMessage,
        hasFiltered,
        hasSearched,
        hasReset
      ];
}
