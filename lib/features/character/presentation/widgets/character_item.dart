import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/extension/extension.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';
class CharacterItemWidget extends StatelessWidget {
  const CharacterItemWidget({
    super.key,
    required this.character,
    this.hasImage = true,
    this.onTap,
  });

  final CharacterEntity character;
  final bool hasImage;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      leading: hasImage
          ? CachedNetworkImageWidget(
              width: context.width * 0.2133,
              height: context.height * 0.0985,
              radius: BorderRadius.circular(12),
              imageUrl: character.image,
            )
          : null,
      title: Text(character.name),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(character.species),
          Text(
            character.gender.checkGender(),
            style: const TextStyle(color: AppColors.kOrange),
          ),
        ],
      ),
      trailing: character.status.checkStatus(),
      onTap: onTap,
    );
  }
}
