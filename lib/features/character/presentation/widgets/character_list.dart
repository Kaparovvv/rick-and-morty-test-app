import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/config/router/app_router.gr.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/presentation/blocs/character/character_bloc.dart';
import 'package:rick_and_morty_app/features/character/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

class CharacterListWidget extends StatefulWidget {
  const CharacterListWidget({super.key});

  @override
  State<CharacterListWidget> createState() => _CharacterListWidgetState();
}

class _CharacterListWidgetState extends State<CharacterListWidget> {
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_onScroll);
  }

  void _onScroll() {
    if (_isBottom) _fetchCharacters();
  }

  void _fetchCharacters({int? page}) {
    context.read<CharacterBloc>().add(FetchCharactersEvent(page: page));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CharacterBloc, CharacterState>(
      listener: (context, state) {
        if (state.status == CharacterBlocStatus.noCharacter &&
            state.hasFiltered) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(LocaleKeys.bad_request_no_character.tr()),
            ),
          );
        }
      },
      builder: (context, state) {
        if (state.status == CharacterBlocStatus.initial) {
          return const Center(child: CircularProgressIndicator.adaptive());
        }
        if (state.status == CharacterBlocStatus.failure) {
          return ErrorMessageWidget(
            message: state.errorMessage,
            onTap: () => _fetchCharacters(page: 1),
          );
        }
        if (state.status == CharacterBlocStatus.noCharacter &&
            state.hasSearched) {
          return NotContentWidget(
            message: LocaleKeys.bad_request_no_character.tr(),
          );
        }
        final characters = state.characters;
        final isOperationComplete =
            state.hasReachedMax || state.hasFiltered || state.hasSearched;
        return RefreshIndicator.adaptive(
          onRefresh: () async => _onRefresh(context.read<CharacterBloc>()),
          child: CustomScrollView(
            controller: _scrollController,
            slivers: [
              const SliverGap(24),
              SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                sliver: SliverList.separated(
                  itemCount: isOperationComplete
                      ? state.characters.length
                      : state.characters.length + 1,
                  itemBuilder: (context, index) {
                    if (state.hasFiltered) {
                      return CharacterItemWidget(
                        character: characters[index],
                        onTap: () => context.router.push(
                          CharacterRoute(character: characters[index]),
                        ),
                      );
                    }
                    return index >= state.characters.length
                        ? const CustomProgressIndicator()
                        : CharacterItemWidget(
                            character: characters[index],
                            onTap: () => context.router.push(
                              CharacterRoute(character: characters[index]),
                            ),
                          );
                  },
                  separatorBuilder: (context, index) {
                    return const Gap(12);
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Future<void> _onRefresh(CharacterBloc bloc) async {
    final completer = Completer();
    bloc.add(RefreshCharactersEvent(completer: completer));
    return completer.future;
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }
}
