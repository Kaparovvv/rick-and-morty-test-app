import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/enums/enums.dart';
import 'package:rick_and_morty_app/core/extension/extension.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';
import 'package:rick_and_morty_app/features/character/presentation/blocs/character/character_bloc.dart';
import 'package:rick_and_morty_app/features/character/presentation/blocs/filter_validation/character_filter_validation_bloc.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

class FilterCharacterWidget extends StatelessWidget {
  const FilterCharacterWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CharacterFilterValidationBloc(),
      child: const _FilterCharacterWidget(),
    );
  }
}

class _FilterCharacterWidget extends StatefulWidget {
  const _FilterCharacterWidget();

  @override
  State<_FilterCharacterWidget> createState() => __FilterCharacterBottotState();
}

class __FilterCharacterBottotState extends State<_FilterCharacterWidget> {
  late FocusNode _nameFocus, _speciesFocus, _typeFocus;

  @override
  void initState() {
    initialize();
    super.initState();
  }

  void initialize() {
    _nameFocus = FocusNode();
    _speciesFocus = FocusNode();
    _typeFocus = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    final t = Theme.of(context);
    return BlocBuilder<CharacterFilterValidationBloc,
        CharacterFilterValidationState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(24),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Gap(context.viewPadding.top),
                CustomTextFieldWidget(
                  hinText: LocaleKeys.character_filter_name.tr(),
                  focusNode: _nameFocus,
                  onTapOutside: (_) => _nameFocus.unfocus(),
                  onChanged: (p0) {
                    context
                        .read<CharacterFilterValidationBloc>()
                        .add(CharacterNameChangedEvent(p0));
                  },
                ),
                const Gap(16),
                CustomTextFieldWidget(
                  hinText: LocaleKeys.character_filter_species.tr(),
                  focusNode: _speciesFocus,
                  onTapOutside: (_) => _speciesFocus.unfocus(),
                  onChanged: (p0) {
                    context
                        .read<CharacterFilterValidationBloc>()
                        .add(CharacterSpeciesChangedEvent(p0));
                  },
                ),
                const Gap(16),
                CustomTextFieldWidget(
                  hinText: LocaleKeys.character_filter_type.tr(),
                  focusNode: _typeFocus,
                  onTapOutside: (_) => _typeFocus.unfocus(),
                  onChanged: (p0) {
                    context
                        .read<CharacterFilterValidationBloc>()
                        .add(CharacterTypeChangedEvent(p0));
                  },
                ),
                const Gap(16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: DropdownMenu<CharacterStatus>(
                        hintText: LocaleKeys.character_filter_status.tr(),
                        onSelected: (p0) {
                          if (p0 == null) return;
                          context
                              .read<CharacterFilterValidationBloc>()
                              .add(CharacterStatusChangedEvent(p0.key));
                        },
                        dropdownMenuEntries: CharacterStatus.values
                            .map<DropdownMenuEntry<CharacterStatus>>(
                                (CharacterStatus value) {
                          return DropdownMenuEntry<CharacterStatus>(
                            value: value,
                            label: value.name,
                          );
                        }).toList(),
                      ),
                    ),
                    const Gap(16),
                    Flexible(
                      child: DropdownMenu<CharacterGender>(
                        hintText: LocaleKeys.character_filter_gender.tr(),
                        onSelected: (p0) {
                          if (p0 == null) return;
                          context
                              .read<CharacterFilterValidationBloc>()
                              .add(CharacterGenderChangedEvent(p0.key));
                        },
                        dropdownMenuEntries: CharacterGender.values
                            .map<DropdownMenuEntry<CharacterGender>>(
                                (CharacterGender value) {
                          return DropdownMenuEntry<CharacterGender>(
                            value: value,
                            label: value.name,
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
                const Gap(32),
                ElevatedButton(
                  onPressed: state.isValid ? () => _onFilter(state) : null,
                  child: Text(
                    LocaleKeys.search.tr(),
                    style: t.textTheme.titleMedium
                        ?.copyWith(color: AppColors.kWhite),
                  ),
                ),
                Gap(context.viewPadding.bottom),
                Gap(context.viewInsets.bottom),
              ],
            ),
          ),
        );
      },
    );
  }

  void _onFilter(CharacterFilterValidationState state) {
    context.router.maybePop();
    context.read<CharacterBloc>().add(
          FilterCharactersEvent(
            filterQueries: FilterCharacterEntity(
              name: state.name.value,
              species: state.species.value,
              type: state.type.value,
              gender: state.gender.value,
              status: state.characteStatus.value,
            ),
          ),
        );
  }

  @override
  void dispose() {
    super.dispose();
    _nameFocus.dispose();
    _speciesFocus.dispose();
    _typeFocus.dispose();
  }
}
