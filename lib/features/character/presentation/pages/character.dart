import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/core/extension/extension.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

@RoutePage()
class CharacterPage extends StatelessWidget {
  const CharacterPage({super.key, required this.character});

  final CharacterEntity character;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(character.name),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CachedNetworkImage(
              imageUrl: character.image,
              width: double.infinity,
              height: context.width / 2,
              fit: BoxFit.fitWidth,
            ),
            const Gap(16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  LabeledValueWidget(
                    label: LocaleKeys.character_filter_species.tr(),
                    value: character.species,
                  ),
                  const Gap(6),
                  LabeledValueWidget(
                    label: LocaleKeys.character_filter_gender.tr(),
                    value: character.gender.checkGender(),
                  ),
                  const Gap(6),
                  if (character.status.checkStatus().data != null) ...[
                    LabeledValueWidget(
                      label: LocaleKeys.character_filter_status.tr(),
                      value: character.status.checkStatus().data!,
                    ),
                    const Gap(6),
                  ],
                  LabeledValueWidget(
                    label: LocaleKeys.character_origin.tr(),
                    value: character.origin.name,
                  ),
                  const Gap(6),
                  LabeledValueWidget(
                    label: LocaleKeys.character_location.tr(),
                    value: character.location.name,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
