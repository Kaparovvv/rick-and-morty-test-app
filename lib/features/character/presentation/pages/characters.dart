import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/gen/assets.gen.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/di/injection.dart';
import 'package:rick_and_morty_app/features/character/presentation/blocs/character/character_bloc.dart';
import 'package:rick_and_morty_app/features/character/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

@RoutePage()
class CharactersPage extends StatelessWidget {
  const CharactersPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          CharacterBloc(getIt(), getIt())..add(const FetchCharactersEvent()),
      child: const _CharactersPage(),
    );
  }
}

class _CharactersPage extends StatefulWidget {
  const _CharactersPage();

  @override
  State<_CharactersPage> createState() => __CharactersPageState();
}

class __CharactersPageState extends State<_CharactersPage> {
  late FocusNode _searchFocus;
  late TextEditingController _searchController;

  @override
  void initState() {
    _searchFocus = FocusNode();
    _searchController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final t = Theme.of(context);
    return BlocBuilder<CharacterBloc, CharacterState>(
        builder: (context, state) {
      final shouldDisplayResults =
          state.status == CharacterBlocStatus.success && !state.hasFiltered ||
              state.status == CharacterBlocStatus.noCharacter;
      return Scaffold(
        appBar: AppBar(
          title: Text(LocaleKeys.characters.tr()),
          actions: shouldDisplayResults
              ? [
                  IconButton(
                    onPressed: () => _showFilter(context),
                    icon: Assets.icons.icFilter.svg(),
                  ),
                ]
              : null,
          bottom: shouldDisplayResults
              ? PreferredSize(
                  preferredSize: const Size(double.infinity, kToolbarHeight),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24.0,
                      vertical: 12,
                    ),
                    child: SearchFieldWidget(
                      controller: _searchController,
                      focusNode: _searchFocus,
                      hasReset: state.hasReset,
                      onReset: _onReset,
                      onChanged: _onChanged,
                    ),
                  ),
                )
              : null,
        ),
        body: const CharacterListWidget(),
        floatingActionButtonLocation:
            state.hasFiltered ? FloatingActionButtonLocation.centerFloat : null,
        floatingActionButton: state.hasFiltered
            ? Padding(
                padding: const EdgeInsets.all(24),
                child: ElevatedButton(
                  onPressed: () => context
                      .read<CharacterBloc>()
                      .add(const ResetFilterOrSearchCharacterEvent()),
                  child: Text(
                    LocaleKeys.reset_filter.tr(),
                    style: t.textTheme.titleMedium
                        ?.copyWith(color: AppColors.kWhite),
                  ),
                ),
              )
            : null,
      );
    });
  }

  void _onReset() {
    _searchController.clear();
    context.read<CharacterBloc>().add(const ResetFilterOrSearchCharacterEvent());
  }

  void _onChanged(String value) {
    context.read<CharacterBloc>().add(SearchCharacterEvent(query: value));
  }

  void _showFilter(BuildContext localContext) {
    showModalBottomSheet(
      context: context,
      showDragHandle: true,
      isScrollControlled: true,
      useSafeArea: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
      ),
      builder: (context) {
        return BlocProvider.value(
          value: BlocProvider.of<CharacterBloc>(localContext),
          child: const FilterCharacterWidget(),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _searchFocus.dispose();
    _searchController.dispose();
  }
}
