import 'package:dio/dio.dart';
import 'package:rick_and_morty_app/app/data/data_state/data_state.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/data/datasources/remote/character_api_service.dart';
import 'package:rick_and_morty_app/features/character/data/models/models.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';
import 'package:rick_and_morty_app/features/character/domain/repositories/character.dart';

@Singleton(as: CharacterRepository)
class CharacterRepoImpl implements CharacterRepository {
  CharacterRepoImpl(this._apiService);

  final CharacterApiService _apiService;

  @override
  Future<DataState<CharactersEntity>> fetchCharacters(int page) async {
    try {
      final httpResponse = await _apiService.fetchCharacters(
        page,
      );
      if (httpResponse.response.statusCode == 200) {
        return DataSuccess(httpResponse.data);
      } else {
        return DataError(
          DioException(
            error: httpResponse.response.statusMessage,
            response: httpResponse.response,
            type: DioExceptionType.badResponse,
            requestOptions: httpResponse.response.requestOptions,
          ),
        );
      }
    } catch (e) {
      return DataError(e);
    }
  }

  @override
  Future<DataState<CharactersEntity>> filterCharacters(
    FilterCharacterEntity queries,
  ) async {
    try {
      final filterQueries = FilterCharacterRequestModel.fromEntity(queries);
      final httpResponse = await _apiService.filterCharacters(filterQueries);
      if (httpResponse.response.statusCode == 200) {
        return DataSuccess(httpResponse.data);
      } else {
        return DataError(
          DioException(
            error: httpResponse.response.statusMessage,
            response: httpResponse.response,
            type: DioExceptionType.badResponse,
            requestOptions: httpResponse.response.requestOptions,
          ),
        );
      }
    } catch (e) {
      return DataError(e);
    }
  }
}
