import 'package:dio/dio.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/character/data/models/models.dart';

part 'character_api_service.g.dart';

@singleton
@RestApi()
abstract class CharacterApiService {
  @factoryMethod
  factory CharacterApiService(Dio dio) = _CharacterApiService;

  @GET('/character')
  Future<HttpResponse<CharactersModel>> fetchCharacters(
    @Query('page') int page,
  );

  @GET('/character')
  Future<HttpResponse<CharactersModel>> filterCharacters(
    @Queries() FilterCharacterRequestModel filterQueries,
  );
}
