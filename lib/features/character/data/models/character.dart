import 'package:rick_and_morty_app/core/enums/enums.dart';
import 'package:rick_and_morty_app/features/character/data/models/models.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';

class CharacterModel extends CharacterEntity {
  const CharacterModel({
    required super.id,
    required super.name,
    required super.status,
    required super.species,
    required super.type,
    required super.gender,
    required super.origin,
    required super.location,
    required super.image,
    required super.episode,
    required super.url,
    required super.created,
  });

  CharacterModel copyWith({
    int? id,
    String? name,
    CharacterStatus? status,
    String? species,
    String? type,
    CharacterGender? gender,
    CharacterOriginModel? origin,
    CharacterLocationModel? location,
    String? image,
    List<String>? episode,
    String? url,
    DateTime? created,
  }) =>
      CharacterModel(
        id: id ?? this.id,
        name: name ?? this.name,
        status: status ?? this.status,
        species: species ?? this.species,
        type: type ?? this.type,
        gender: gender ?? this.gender,
        origin: origin ?? this.origin,
        location: location ?? this.location,
        image: image ?? this.image,
        episode: episode ?? this.episode,
        url: url ?? this.url,
        created: created ?? this.created,
      );

  factory CharacterModel.fromJson(Map<String, dynamic> json) => CharacterModel(
        id: json["id"],
        name: json["name"],
        status: _status.map[json["status"]] ?? CharacterStatus.unknown,
        species: json["species"],
        type: json["type"],
        gender: _gender.map[json["gender"]] ?? CharacterGender.unknown,
        origin: CharacterOriginModel.fromJson(json["origin"]),
        location: CharacterLocationModel.fromJson(json["location"]),
        image: json["image"],
        episode: List<String>.from(json["episode"].map((x) => x)),
        url: json["url"],
        created: DateTime.parse(json["created"]),
      );
}

final _gender = EnumValues({
  "Male": CharacterGender.male,
  "Female": CharacterGender.female,
  "Genderless": CharacterGender.genderless,
  "unknown": CharacterGender.unknown,
});

final _status = EnumValues({
  "Alive": CharacterStatus.alive,
  "Dead": CharacterStatus.dead,
  "unknown": CharacterStatus.unknown,
});
