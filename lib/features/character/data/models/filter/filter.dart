import 'package:json_annotation/json_annotation.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';

part 'filter.g.dart';

@JsonSerializable()
class FilterCharacterRequestModel {
  const FilterCharacterRequestModel({
    required this.name,
    required this.status,
    required this.species,
    required this.type,
    required this.gender,
  });

  final String name, species, type, gender, status;

  FilterCharacterRequestModel copyWith({
    String? name,
    String? status,
    String? species,
    String? type,
    String? gender,
  }) =>
      FilterCharacterRequestModel(
        name: name ?? this.name,
        status: status ?? this.status,
        species: species ?? this.species,
        type: type ?? this.type,
        gender: gender ?? this.gender,
      );

  factory FilterCharacterRequestModel.fromEntity(
          FilterCharacterEntity entity) =>
      FilterCharacterRequestModel(
        name: entity.name,
        status: entity.status,
        species: entity.species,
        gender: entity.gender,
        type: entity.type,
      );

  factory FilterCharacterRequestModel.fromJson(Map<String, dynamic> json) =>
      _$FilterCharacterRequestModelFromJson(json);

  Map<String, dynamic> toJson() => _$FilterCharacterRequestModelToJson(this);
}
