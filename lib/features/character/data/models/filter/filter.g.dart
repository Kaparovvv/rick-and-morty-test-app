// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilterCharacterRequestModel _$FilterCharacterRequestModelFromJson(
        Map<String, dynamic> json) =>
    FilterCharacterRequestModel(
      name: json['name'] as String,
      status: json['status'] as String,
      species: json['species'] as String,
      type: json['type'] as String,
      gender: json['gender'] as String,
    );

Map<String, dynamic> _$FilterCharacterRequestModelToJson(
        FilterCharacterRequestModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'species': instance.species,
      'type': instance.type,
      'gender': instance.gender,
      'status': instance.status,
    };
