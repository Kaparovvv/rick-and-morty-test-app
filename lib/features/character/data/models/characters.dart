import 'package:rick_and_morty_app/app/data/models/models.dart';
import 'package:rick_and_morty_app/features/character/data/models/character.dart';
import 'package:rick_and_morty_app/features/character/domain/entities/characters.dart';

class CharactersModel extends CharactersEntity {
  const CharactersModel({
    required super.info,
    required super.results,
  });

  CharactersModel copyWith({
    InfoModel? info,
    List<CharacterModel>? results,
  }) =>
      CharactersModel(
        info: info ?? this.info,
        results: results ?? this.results,
      );

  factory CharactersModel.fromJson(Map<String, dynamic> json) =>
      CharactersModel(
        info: InfoModel.fromJson(json["info"]),
        results: List<CharacterModel>.from(
            json["results"].map((x) => CharacterModel.fromJson(x))),
      );
}
