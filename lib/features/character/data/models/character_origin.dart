import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';

class CharacterOriginModel extends CharacterOriginEntity {
  const CharacterOriginModel({
    required super.name,
    required super.url,
  });

  CharacterOriginModel copyWith({
    String? name,
    String? url,
  }) =>
      CharacterOriginModel(
        name: name ?? this.name,
        url: url ?? this.url,
      );

  factory CharacterOriginModel.fromJson(Map<String, dynamic> json) =>
      CharacterOriginModel(
        name: json["name"],
        url: json["url"],
      );
}
