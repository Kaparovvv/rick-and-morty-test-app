import 'package:rick_and_morty_app/features/character/domain/entities/entities.dart';

class CharacterLocationModel extends CharacterLocationEntity {
  const CharacterLocationModel({
    required super.name,
    required super.url,
  });

  CharacterLocationModel copyWith({
    String? name,
    String? url,
  }) =>
      CharacterLocationModel(
        name: name ?? this.name,
        url: url ?? this.url,
      );

  factory CharacterLocationModel.fromJson(Map<String, dynamic> json) =>
      CharacterLocationModel(
        name: json["name"],
        url: json["url"],
      );
}
