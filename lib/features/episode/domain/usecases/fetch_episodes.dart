import 'package:rick_and_morty_app/app/data/data_state/data_state.dart';
import 'package:rick_and_morty_app/app/data/usecase/usecase.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';
import 'package:rick_and_morty_app/features/episode/domain/repositories/episode.dart';

@singleton
class FetchEpisodesUseCase implements UseCase<DataState<EpisodesEntity>, int> {
  FetchEpisodesUseCase(this._repository);

  final EpisodeRepository _repository;

  @override
  Future<DataState<EpisodesEntity>> call({required int params}) async {
    return await _repository.fetchEpisodes(params);
  }
}
