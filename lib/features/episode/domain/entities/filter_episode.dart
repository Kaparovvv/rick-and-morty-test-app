import 'package:rick_and_morty_app/core/packages/packages.dart';

class FilterEpisodeEntity extends Equatable {
  const FilterEpisodeEntity({required this.name, required this.episode});

  final String name, episode;

  @override
  List<Object> get props => [name, episode];
}
