import 'package:rick_and_morty_app/app/domain/entities/entities.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';

class EpisodesEntity extends Equatable {
  final InfoEntity info;
  final List<EpisodeEntity> results;

  const EpisodesEntity({
    required this.info,
    required this.results,
  });

  @override
  List<Object?> get props => [info, results];
}
