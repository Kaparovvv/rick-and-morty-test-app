import 'package:rick_and_morty_app/core/packages/packages.dart';

class EpisodeEntity extends Equatable {
  final int id;
  final String name, airDate, episode, url;
  final List<String> characters;
  final DateTime created;

  const EpisodeEntity({
    required this.id,
    required this.name,
    required this.airDate,
    required this.episode,
    required this.characters,
    required this.url,
    required this.created,
  });

  @override
  List<Object> get props {
    return [
      id,
      name,
      airDate,
      episode,
      characters,
      url,
      created,
    ];
  }
}
