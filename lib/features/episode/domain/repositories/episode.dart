import 'package:rick_and_morty_app/app/data/data_state/data_state.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';

abstract class EpisodeRepository {
  Future<DataState<EpisodesEntity>> fetchEpisodes(int page);
  Future<DataState<EpisodesEntity>> filterEpisodes(FilterEpisodeEntity params);
}
