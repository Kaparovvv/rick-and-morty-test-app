// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilterEpisodeRequestModel _$FilterEpisodeRequestModelFromJson(
        Map<String, dynamic> json) =>
    FilterEpisodeRequestModel(
      name: json['name'] as String,
      episode: json['episode'] as String,
    );

Map<String, dynamic> _$FilterEpisodeRequestModelToJson(
        FilterEpisodeRequestModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'episode': instance.episode,
    };
