import 'package:json_annotation/json_annotation.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';

part 'filter.g.dart';

@JsonSerializable()
class FilterEpisodeRequestModel {
  const FilterEpisodeRequestModel({
    required this.name,
    required this.episode,
  });

  final String name, episode;

  FilterEpisodeRequestModel copyWith({
    String? name,
    String? episode,
  }) =>
      FilterEpisodeRequestModel(
        name: name ?? this.name,
        episode: episode ?? this.episode,
      );

  factory FilterEpisodeRequestModel.fromEntity(FilterEpisodeEntity entity) =>
      FilterEpisodeRequestModel(
        name: entity.name,
        episode: entity.episode,
      );

  factory FilterEpisodeRequestModel.fromJson(Map<String, dynamic> json) =>
      _$FilterEpisodeRequestModelFromJson(json);

  Map<String, dynamic> toJson() => _$FilterEpisodeRequestModelToJson(this);
}
