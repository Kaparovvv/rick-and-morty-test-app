import 'package:rick_and_morty_app/app/data/models/models.dart';
import 'package:rick_and_morty_app/features/episode/data/models/models.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';

class EpisodesModel extends EpisodesEntity {
  const EpisodesModel({
    required super.info,
    required super.results,
  });

  EpisodesModel copyWith({
    InfoModel? info,
    List<EpisodeModel>? results,
  }) =>
      EpisodesModel(
        info: info ?? this.info,
        results: results ?? this.results,
      );

  factory EpisodesModel.fromJson(Map<String, dynamic> json) => EpisodesModel(
        info: InfoModel.fromJson(json["info"]),
        results: List<EpisodeModel>.from(
          json["results"].map((x) => EpisodeModel.fromJson(x)),
        ),
      );
}
