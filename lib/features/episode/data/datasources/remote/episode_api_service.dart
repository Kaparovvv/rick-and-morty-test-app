import 'package:dio/dio.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/data/models/models.dart';

part 'episode_api_service.g.dart';

@singleton
@RestApi()
abstract class EpisodeApiService {
  @factoryMethod
  factory EpisodeApiService(Dio dio) = _EpisodeApiService;

  @GET('/episode')
  Future<HttpResponse<EpisodesModel>> fetchEpisodes(
    @Query('page') int page,
  );

  @GET('/episode')
  Future<HttpResponse<EpisodesModel>> filterEpisodes(
    @Queries() FilterEpisodeRequestModel filterQueries,
  );
}
