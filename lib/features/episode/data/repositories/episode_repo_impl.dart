import 'package:dio/dio.dart';
import 'package:rick_and_morty_app/app/data/data_state/data_state.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/data/datasources/remote/episode_api_service.dart';
import 'package:rick_and_morty_app/features/episode/data/models/models.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/episodes.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/filter_episode.dart';
import 'package:rick_and_morty_app/features/episode/domain/repositories/episode.dart';

@Singleton(as: EpisodeRepository)
class EpisodeRepoImpl implements EpisodeRepository {
  EpisodeRepoImpl(this._apiService);

  final EpisodeApiService _apiService;

  @override
  Future<DataState<EpisodesModel>> fetchEpisodes(int page) async {
    try {
      final httpResponse = await _apiService.fetchEpisodes(page);
      if (httpResponse.response.statusCode == 200) {
        return DataSuccess(httpResponse.data);
      } else {
        return DataError(
          DioException(
            error: httpResponse.response.statusMessage,
            response: httpResponse.response,
            type: DioExceptionType.badResponse,
            requestOptions: httpResponse.response.requestOptions,
          ),
        );
      }
    } catch (e) {
      return DataError(e);
    }
  }

  @override
  Future<DataState<EpisodesEntity>> filterEpisodes(FilterEpisodeEntity params) async {
    try {
      final filterQueries = FilterEpisodeRequestModel.fromEntity(params);
      final httpResponse = await _apiService.filterEpisodes(filterQueries);
      if (httpResponse.response.statusCode == 200) {
        return DataSuccess(httpResponse.data);
      } else {
        return DataError(
          DioException(
            error: httpResponse.response.statusMessage,
            response: httpResponse.response,
            type: DioExceptionType.badResponse,
            requestOptions: httpResponse.response.requestOptions,
          ),
        );
      }
    } catch (e) {
      return DataError(e);
    }
  }
}
