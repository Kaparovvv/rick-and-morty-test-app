import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

@RoutePage()
class EpisodePage extends StatelessWidget {
  const EpisodePage({super.key, required this.episode});

  final EpisodeEntity episode;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(episode.episode),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24),
          child: Column(
            children: [
              LabeledValueWidget(
                label: LocaleKeys.character_filter_name.tr(),
                value: episode.name,
              ),
              const Gap(6),
              LabeledValueWidget(
                label: LocaleKeys.episode.tr(),
                value: episode.episode,
              ),
              const Gap(6),
              LabeledValueWidget(
                label: LocaleKeys.air_date.tr(),
                value: episode.airDate,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
