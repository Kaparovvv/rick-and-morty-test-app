import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/gen/assets.gen.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/di/injection.dart';
import 'package:rick_and_morty_app/features/episode/presentation/blocs/episode/episode_bloc.dart';
import 'package:rick_and_morty_app/features/episode/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

@RoutePage()
class EpisodesPage extends StatelessWidget {
  const EpisodesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          EpisodeBloc(getIt(), getIt())..add(const FetchEpisodesEvent()),
      child: const _EpisodesPage(),
    );
  }
}

class _EpisodesPage extends StatefulWidget {
  const _EpisodesPage();

  @override
  State<_EpisodesPage> createState() => __EpisodesPageState();
}

class __EpisodesPageState extends State<_EpisodesPage> {
  late FocusNode _searchFocus;
  late TextEditingController _searchController;

  @override
  void initState() {
    _searchFocus = FocusNode();
    _searchController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final t = Theme.of(context);
    return BlocBuilder<EpisodeBloc, EpisodeState>(
      builder: (context, state) {
        final shouldDisplayResults =
            state.status == EpisodeBlocStatus.success && !state.hasFiltered ||
                state.status == EpisodeBlocStatus.noEpisode;
        return Scaffold(
          appBar: AppBar(
            title: Text(LocaleKeys.episodes.tr()),
            actions: shouldDisplayResults
                ? [
                    IconButton(
                      onPressed: () => _showFilter(context),
                      icon: Assets.icons.icFilter.svg(),
                    ),
                  ]
                : null,
            bottom: shouldDisplayResults
                ? PreferredSize(
                    preferredSize: const Size(double.infinity, kToolbarHeight),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 24.0,
                        vertical: 12,
                      ),
                      child: SearchFieldWidget(
                        controller: _searchController,
                        focusNode: _searchFocus,
                        hasReset: state.hasReset,
                        onReset: _onReset,
                        onChanged: _onChanged,
                      ),
                    ),
                  )
                : null,
          ),
          body: const EpisodeListWidget(),
          floatingActionButtonLocation: state.hasFiltered
              ? FloatingActionButtonLocation.centerFloat
              : null,
          floatingActionButton: state.hasFiltered
              ? Padding(
                  padding: const EdgeInsets.all(24),
                  child: ElevatedButton(
                    onPressed: () => context
                        .read<EpisodeBloc>()
                        .add(const ResetTheFilterOrSearchEpisodeEvent()),
                    child: Text(
                      LocaleKeys.reset_filter.tr(),
                      style: t.textTheme.titleMedium
                          ?.copyWith(color: AppColors.kWhite),
                    ),
                  ),
                )
              : null,
        );
      },
    );
  }

  void _onReset() {
    _searchController.clear();
    context.read<EpisodeBloc>().add(const ResetTheFilterOrSearchEpisodeEvent());
  }

  void _onChanged(String value) {
    context.read<EpisodeBloc>().add(EpisodeSearchEvent(query: value));
  }

  void _showFilter(BuildContext localContext) {
    showModalBottomSheet(
      context: context,
      showDragHandle: true,
      isScrollControlled: true,
      useSafeArea: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
      ),
      builder: (context) {
        return BlocProvider.value(
          value: BlocProvider.of<EpisodeBloc>(localContext),
          child: const FilterEpisodesWidget(),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _searchFocus.dispose();
    _searchController.dispose();
  }
}
