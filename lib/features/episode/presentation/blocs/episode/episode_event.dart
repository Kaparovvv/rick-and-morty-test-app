part of 'episode_bloc.dart';

abstract class EpisodeEvent extends Equatable {
  const EpisodeEvent();

  @override
  List<Object?> get props => [];
}

final class FetchEpisodesEvent extends EpisodeEvent {
  const FetchEpisodesEvent({this.page});

  final int? page;

  @override
  List<Object?> get props => [page];
}

final class RefreshEpisodesEvent extends EpisodeEvent {
  const RefreshEpisodesEvent({this.completer});

  final Completer? completer;

  @override
  List<Object?> get props => [completer];
}

final class EpisodesFilterEvent extends EpisodeEvent {
  const EpisodesFilterEvent(this.queries);

  final FilterEpisodeEntity queries;

  @override
  List<Object?> get props => [queries];
}

final class EpisodeSearchEvent extends EpisodeEvent {
  const EpisodeSearchEvent({required this.query});

  final String query;

  @override
  List<Object?> get props => [query];
}

final class ResetTheFilterOrSearchEpisodeEvent extends EpisodeEvent {
  const ResetTheFilterOrSearchEpisodeEvent();
}
