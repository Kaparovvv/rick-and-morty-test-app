import 'dart:async';
import 'package:dio/dio.dart';
import 'package:rick_and_morty_app/app/data/data_state/data_state.dart';
import 'package:rick_and_morty_app/app/data/error_handler/error_handler.dart';
import 'package:rick_and_morty_app/core/droppables/droppables.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';
import 'package:rick_and_morty_app/features/episode/domain/usecases/fetch_episodes.dart';
import 'package:rick_and_morty_app/features/episode/domain/usecases/filter_episode.dart';

part 'episode_event.dart';
part 'episode_state.dart';

class EpisodeBloc extends Bloc<EpisodeEvent, EpisodeState> {
  EpisodeBloc(
    this._fetchEpisodesUseCase,
    this._filterEpisodesUseCase,
  ) : super(const EpisodeState()) {
    on<FetchEpisodesEvent>(
      _onFetch,
      transformer: throttleDroppable(const Duration(microseconds: 100)),
    );
    on<RefreshEpisodesEvent>(
      _onRefresh,
      transformer: throttleDroppable(const Duration(microseconds: 100)),
    );
    on<EpisodesFilterEvent>(_onFilter);
    on<EpisodeSearchEvent>(_onSearch);
    on<ResetTheFilterOrSearchEpisodeEvent>(_onResetFilterOrSearch);
  }

  final FetchEpisodesUseCase _fetchEpisodesUseCase;
  final FilterEpisodesUseCase _filterEpisodesUseCase;
  int _page = 1;
  List<EpisodeEntity> _episodes = [];

  Future<void> _onRefresh(
    RefreshEpisodesEvent event,
    Emitter<EpisodeState> emit,
  ) async {
    try {
      _page = 1;
      final result = await _fetchEpisodesUseCase.call(params: _page);
      _page++;
      if (result is DataSuccess && result.data != null) {
        _episodes = result.data!.results;
        return emit(state.copyWith(
          status: EpisodeBlocStatus.success,
          episodes: result.data?.results,
          hasReachedMax: false,
          hasFiltered: false,
        ));
      }
      if (result is DataError) {
        final handle = ErrorHandler.handle(result.error);
        final message = handle.failure.message;
        return emit(state.copyWith(
          status: EpisodeBlocStatus.failure,
          errorMessage: message,
        ));
      }
    } finally {
      event.completer?.complete();
    }
  }

  Future<void> _onFetch(
    FetchEpisodesEvent event,
    Emitter<EpisodeState> emit,
  ) async {
    if (state.hasReachedMax) return;

    if (state.status == EpisodeBlocStatus.initial) {
      final result = await _fetchEpisodesUseCase.call(params: _page);
      _page++;
      if (result is DataSuccess && result.data != null) {
        _episodes = result.data!.results;
        return emit(state.copyWith(
          status: EpisodeBlocStatus.success,
          episodes: result.data?.results,
          hasReachedMax: false,
          hasFiltered: false,
        ));
      }
      if (result is DataError) {
        final handle = ErrorHandler.handle(result.error);
        final message = handle.failure.message;
        return emit(state.copyWith(
          status: EpisodeBlocStatus.failure,
          errorMessage: message,
        ));
      }
    }
    final result =
        await _fetchEpisodesUseCase.call(params: event.page ?? _page);
    if (result is DataSuccess && result.data != null) {
      final data = result.data!;
      final episodes = data.results;
      _page++;
      _episodes = episodes;
      emit(state.copyWith(
        status: EpisodeBlocStatus.success,
        episodes: List.of(state.episodes)..addAll(episodes),
        hasReachedMax: result.data!.info.next == null,
        hasFiltered: false,
        hasSearched: false,
      ));
    }
    if (result is DataError) {
      final handle = ErrorHandler.handle(result.error);
      final message = handle.failure.message;
      return emit(state.copyWith(
        status: EpisodeBlocStatus.failure,
        errorMessage: message,
      ));
    }
  }

  Future<void> _onFilter(
    EpisodesFilterEvent event,
    Emitter<EpisodeState> emit,
  ) async {
    emit(state.copyWith(status: EpisodeBlocStatus.initial));
    final result = await _filterEpisodesUseCase.call(params: event.queries);

    if (result is DataSuccess) {
      return emit(state.copyWith(
        status: EpisodeBlocStatus.success,
        episodes: result.data?.results,
        hasFiltered: true,
        hasReachedMax: false,
      ));
    }
    if (result is DataError) {
      if (result.error is DioException) {
        final error = result.error as DioException;
        if (error.response?.statusCode == 404) {
          emit(state.copyWith(
            status: EpisodeBlocStatus.noEpisode,
            hasFiltered: true,
          ));
          add(const ResetTheFilterOrSearchEpisodeEvent());
        }
      }
      final handle = ErrorHandler.handle(result.error);
      final message = handle.failure.message;
      return emit(state.copyWith(
        status: EpisodeBlocStatus.failure,
        errorMessage: message,
      ));
    }
  }

  Future<void> _onSearch(
    EpisodeSearchEvent event,
    Emitter<EpisodeState> emit,
  ) async {
    emit(state.copyWith(status: EpisodeBlocStatus.initial));
    final result = _episodes
        .where((item) => item.name.toLowerCase().contains(
              event.query.toLowerCase(),
            ))
        .toList();

    emit(state.copyWith(
      status: result.isNotEmpty
          ? EpisodeBlocStatus.success
          : EpisodeBlocStatus.noEpisode,
      episodes: result.isNotEmpty ? result : [],
      hasReachedMax: false,
      hasFiltered: false,
      hasSearched: true,
      hasReset: event.query.isNotEmpty,
    ));
  }

  void _onResetFilterOrSearch(
    ResetTheFilterOrSearchEpisodeEvent event,
    Emitter<EpisodeState> emit,
  ) {
    emit(state.copyWith(
      status: EpisodeBlocStatus.success,
      episodes: _episodes,
      hasReachedMax: false,
      hasFiltered: false,
      hasSearched: false,
      hasReset: false,
    ));
  }
}
