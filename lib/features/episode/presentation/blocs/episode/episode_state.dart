part of 'episode_bloc.dart';

enum EpisodeBlocStatus { initial, success, failure, noEpisode }

final class EpisodeState extends Equatable {
  const EpisodeState({
    this.status = EpisodeBlocStatus.initial,
    this.episodes = const <EpisodeEntity>[],
    this.hasReachedMax = false,
    this.errorMessage = '',
    this.hasFiltered = false,
    this.hasSearched = false,
    this.hasReset = false,
  });

  final EpisodeBlocStatus status;
  final List<EpisodeEntity> episodes;
  final bool hasReachedMax;
  final String errorMessage;
  final bool hasFiltered;
  final bool hasSearched;
  final bool hasReset;

  EpisodeState copyWith({
    EpisodeBlocStatus? status,
    List<EpisodeEntity>? episodes,
    bool? hasReachedMax,
    String? errorMessage,
    bool? hasFiltered,
    bool? hasSearched,
    bool? hasReset,
  }) {
    return EpisodeState(
      status: status ?? this.status,
      episodes: episodes ?? this.episodes,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      errorMessage: errorMessage ?? this.errorMessage,
      hasFiltered: hasFiltered ?? this.hasFiltered,
      hasSearched: hasSearched ?? this.hasSearched,
      hasReset: hasReset ?? this.hasReset,
    );
  }

  @override
  String toString() {
    return '''EpisodeState { status: $status, hasReachedMax: $hasReachedMax, episodes: ${episodes.length}, errorMessage: $errorMessage, hasFilteredEpisodes: $hasFiltered, hasSearched: $hasSearched, hasReset: $hasReset}''';
  }

  @override
  List<Object?> get props => [
        status,
        episodes,
        hasReachedMax,
        errorMessage,
        hasFiltered,
        hasSearched,
        hasReset
      ];
}
