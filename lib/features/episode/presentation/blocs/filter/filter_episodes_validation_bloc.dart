import 'package:formz/formz.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/presentation/forms/filter_forms.dart';

part 'filter_episodes_validation_event.dart';
part 'filter_episodes_validation_state.dart';

class FilterEpisodesValidationBloc
    extends Bloc<FilterEpisodesValidationEvent, FilterEpisodesValidationState> {
  FilterEpisodesValidationBloc()
      : super(const FilterEpisodesValidationState()) {
    on<EpisodeNameChangedEvent>(_onNameChanged);
    on<EpisodeCodeChangedEvent>(_onCodeChanged);
  }

  void _onNameChanged(
    EpisodeNameChangedEvent event,
    Emitter<FilterEpisodesValidationState> emit,
  ) {
    final name = EpisodeNameForm.dirty(event.name);
    emit(state.copyWith(
      name: name,
      isValidForm: EpisodesFilterForm().isValid,
    ));
  }

  void _onCodeChanged(
    EpisodeCodeChangedEvent event,
    Emitter<FilterEpisodesValidationState> emit,
  ) {
    final code = EpisodeCodeForm.dirty(event.code);
    emit(state.copyWith(
      code: code,
      isValidForm: EpisodesFilterForm().isValid,
    ));
  }
}
