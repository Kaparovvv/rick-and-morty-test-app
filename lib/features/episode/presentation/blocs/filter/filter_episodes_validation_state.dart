part of 'filter_episodes_validation_bloc.dart';

final class FilterEpisodesValidationState extends Equatable with FormzMixin {
  const FilterEpisodesValidationState({
    this.status = FormzSubmissionStatus.initial,
    this.name = const EpisodeNameForm.pure(),
    this.code = const EpisodeCodeForm.pure(),
    this.isValidForm = false,
  });

  final FormzSubmissionStatus status;
  final EpisodeNameForm name;
  final EpisodeCodeForm code;
  final bool isValidForm;

  FilterEpisodesValidationState copyWith({
    FormzSubmissionStatus? status,
    EpisodeNameForm? name,
    EpisodeCodeForm? code,
    bool? isValidForm,
  }) {
    return FilterEpisodesValidationState(
      status: status ?? this.status,
      name: name ?? this.name,
      code: code ?? this.code,
      isValidForm: isValidForm ?? this.isValidForm,
    );
  }

  @override
  List<Object> get props => [status, name, code, isValidForm];
  @override
  List<FormzInput> get inputs => [name, code];
}
