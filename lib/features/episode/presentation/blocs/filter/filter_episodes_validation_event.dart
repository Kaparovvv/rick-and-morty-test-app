part of 'filter_episodes_validation_bloc.dart';

sealed class FilterEpisodesValidationEvent extends Equatable {
  const FilterEpisodesValidationEvent();

  @override
  List<Object> get props => [];
}

final class EpisodeNameChangedEvent extends FilterEpisodesValidationEvent {
  const EpisodeNameChangedEvent(this.name);

  final String name;

  @override
  List<Object> get props => [name];
}

final class EpisodeCodeChangedEvent extends FilterEpisodesValidationEvent {
  const EpisodeCodeChangedEvent(this.code);

  final String code;

  @override
  List<Object> get props => [code];
}
