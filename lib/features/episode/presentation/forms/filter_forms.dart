import 'package:formz/formz.dart';

class EpisodesFilterForm with FormzMixin {
  EpisodesFilterForm({
    this.name = const EpisodeNameForm.pure(),
    this.species = const EpisodeCodeForm.pure(),
  });

  final EpisodeNameForm name;
  final EpisodeCodeForm species;

  @override
  List<FormzInput> get inputs => [name, species];
}

enum EpisodeNameValidationError { empty }

class EpisodeNameForm extends FormzInput<String, EpisodeNameValidationError> {
  const EpisodeNameForm.pure() : super.pure('');
  const EpisodeNameForm.dirty([super.value = '']) : super.dirty();

  @override
  EpisodeNameValidationError? validator(String value) {
    if (value.isEmpty) return EpisodeNameValidationError.empty;
    return null;
  }
}

enum EpisodeCodeValidationError { empty }

class EpisodeCodeForm extends FormzInput<String, EpisodeCodeValidationError> {
  const EpisodeCodeForm.pure() : super.pure('');
  const EpisodeCodeForm.dirty([super.value = '']) : super.dirty();

  @override
  EpisodeCodeValidationError? validator(String value) {
    if (value.isEmpty) return EpisodeCodeValidationError.empty;
    return null;
  }
}
