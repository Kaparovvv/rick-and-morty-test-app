import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/extension/extension.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';
import 'package:rick_and_morty_app/features/episode/presentation/blocs/episode/episode_bloc.dart';
import 'package:rick_and_morty_app/features/episode/presentation/blocs/filter/filter_episodes_validation_bloc.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

class FilterEpisodesWidget extends StatelessWidget {
  const FilterEpisodesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FilterEpisodesValidationBloc(),
      child: const _FilterEpisodesWidget(),
    );
  }
}

class _FilterEpisodesWidget extends StatefulWidget {
  const _FilterEpisodesWidget();

  @override
  State<_FilterEpisodesWidget> createState() => __FilterEpisodesState();
}

class __FilterEpisodesState extends State<_FilterEpisodesWidget> {
  late FocusNode _nameFocus, _codeFocus;

  @override
  void initState() {
    initialize();
    super.initState();
  }

  void initialize() {
    _nameFocus = FocusNode();
    _codeFocus = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    final t = Theme.of(context);
    return BlocBuilder<FilterEpisodesValidationBloc,
        FilterEpisodesValidationState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(24),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Gap(context.viewPadding.top),
                CustomTextFieldWidget(
                  hinText: LocaleKeys.character_filter_name.tr(),
                  focusNode: _nameFocus,
                  onTapOutside: (_) => _nameFocus.unfocus(),
                  onChanged: (p0) {
                    context
                        .read<FilterEpisodesValidationBloc>()
                        .add(EpisodeNameChangedEvent(p0));
                  },
                ),
                const Gap(16),
                CustomTextFieldWidget(
                  hinText: LocaleKeys.episode.tr(),
                  focusNode: _codeFocus,
                  onTapOutside: (_) => _codeFocus.unfocus(),
                  onChanged: (p0) {
                    context
                        .read<FilterEpisodesValidationBloc>()
                        .add(EpisodeCodeChangedEvent(p0));
                  },
                ),
                const Gap(32),
                ElevatedButton(
                  onPressed: state.isValid ? () => _onFilter(state) : null,
                  child: Text(
                    LocaleKeys.search.tr(),
                    style: t.textTheme.titleMedium
                        ?.copyWith(color: AppColors.kWhite),
                  ),
                ),
                Gap(context.viewPadding.bottom),
                Gap(context.viewInsets.bottom),
              ],
            ),
          ),
        );
      },
    );
  }

  void _onFilter(FilterEpisodesValidationState state) {
    context.router.maybePop();
    context.read<EpisodeBloc>().add(
          EpisodesFilterEvent(
            FilterEpisodeEntity(
              name: state.name.value,
              episode: state.code.value,
            ),
          ),
        );
  }

  @override
  void dispose() {
    super.dispose();
    _nameFocus.dispose();
    _codeFocus.dispose();
  }
}
