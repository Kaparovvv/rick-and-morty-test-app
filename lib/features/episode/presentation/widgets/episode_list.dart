import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/config/router/app_router.gr.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/features/episode/presentation/blocs/episode/episode_bloc.dart';
import 'package:rick_and_morty_app/features/episode/presentation/widgets/episode_item.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

class EpisodeListWidget extends StatefulWidget {
  const EpisodeListWidget({super.key});

  @override
  State<EpisodeListWidget> createState() => _EpisodeListWidgetState();
}

class _EpisodeListWidgetState extends State<EpisodeListWidget> {
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_onScroll);
  }

  void _onScroll() {
    if (_isBottom) _fetchEpisodes();
  }

  void _fetchEpisodes({int? page}) {
    context.read<EpisodeBloc>().add(FetchEpisodesEvent(page: page));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<EpisodeBloc, EpisodeState>(
      listener: (context, state) {
        if (state.status == EpisodeBlocStatus.noEpisode && state.hasFiltered) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(LocaleKeys.bad_request_no_episode.tr()),
            ),
          );
        }
      },
      builder: (context, state) {
        if (state.status == EpisodeBlocStatus.initial) {
          return const Center(child: CircularProgressIndicator.adaptive());
        }

        if (state.status == EpisodeBlocStatus.failure) {
          return ErrorMessageWidget(
            message: state.errorMessage,
            onTap: () => _fetchEpisodes(page: 1),
          );
        }

        if (state.status == EpisodeBlocStatus.noEpisode && state.hasSearched) {
          return NotContentWidget(
            message: LocaleKeys.bad_request_no_episode.tr(),
          );
        }

        final episodes = state.episodes;
        final isOperationComplete =
            state.hasReachedMax || state.hasFiltered || state.hasSearched;

        return RefreshIndicator.adaptive(
          onRefresh: () async => _onRefresh(context.read<EpisodeBloc>()),
          child: CustomScrollView(
            controller: _scrollController,
            slivers: [
              const SliverGap(24),
              SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                sliver: SliverList.separated(
                  itemCount: isOperationComplete
                      ? state.episodes.length
                      : state.episodes.length + 1,
                  itemBuilder: (context, index) {
                    if (state.hasFiltered) {
                      return EpisodeItemWidget(
                        episode: episodes[index],
                        onTap: () => context.router.push(
                          EpisodeRoute(episode: episodes[index]),
                        ),
                      );
                    }
                    return index >= state.episodes.length
                        ? const CustomProgressIndicator()
                        : EpisodeItemWidget(
                            episode: episodes[index],
                            onTap: () => context.router.push(
                              EpisodeRoute(episode: episodes[index]),
                            ),
                          );
                  },
                  separatorBuilder: (context, index) {
                    return const Gap(12);
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Future<void> _onRefresh(EpisodeBloc bloc) async {
    final completer = Completer();
    bloc.add(RefreshEpisodesEvent(completer: completer));
    return completer.future;
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }
}
