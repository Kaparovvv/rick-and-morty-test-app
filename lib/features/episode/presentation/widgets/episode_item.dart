import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/features/episode/domain/entities/entities.dart';

class EpisodeItemWidget extends StatelessWidget {
  const EpisodeItemWidget({super.key, required this.episode, this.onTap});

  final EpisodeEntity episode;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    final t = Theme.of(context);
    return ListTile(
      tileColor: AppColors.kGrey300,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      title: Text(
        episode.name,
        style: t.textTheme.bodyLarge?.copyWith(fontWeight: FontWeight.w500),
      ),
      subtitle: Text(episode.airDate),
      trailing: Text(
        episode.episode,
        style: t.textTheme.bodyLarge?.copyWith(fontWeight: FontWeight.w500),
      ),
      onTap: onTap,
    );
  }
}
