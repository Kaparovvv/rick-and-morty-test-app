// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const characters = 'characters';
  static const episodes = 'episodes';
  static const locations = 'locations';
  static const connect_timeout_failed_to_connect_to_the_system_please_check_internet_connection = 'connect_timeout.failed_to_connect_to_the_system_please_check_internet_connection';
  static const connect_timeout = 'connect_timeout';
  static const network_connection_there_is_no_connection = 'network_connection.there_is_no_connection';
  static const network_connection_please_check_your_connection = 'network_connection.please_check_your_connection';
  static const network_connection = 'network_connection';
  static const server_error_a_system_has_occurred = 'server_error.a_system_has_occurred';
  static const server_error = 'server_error';
  static const oops_something_went_wrong_try_again = 'oops_something_went_wrong_try_again';
  static const there_is_nothing_here_yet = 'there_is_nothing_here_yet';
  static const repeat = 'repeat';
  static const character_gender_gender_male = 'character_gender.gender_male';
  static const character_gender_gender_female = 'character_gender.gender_female';
  static const character_gender_genderless = 'character_gender.genderless';
  static const character_gender_unknown = 'character_gender.unknown';
  static const character_gender = 'character_gender';
  static const character_status_alive = 'character_status.alive';
  static const character_status_dead = 'character_status.dead';
  static const character_status_unknown = 'character_status.unknown';
  static const character_status = 'character_status';
  static const character_filter_name = 'character_filter.name';
  static const character_filter_species = 'character_filter.species';
  static const character_filter_type = 'character_filter.type';
  static const character_filter_gender = 'character_filter.gender';
  static const character_filter_status = 'character_filter.status';
  static const character_filter = 'character_filter';
  static const search = 'search';
  static const bad_request_no_character = 'bad_request.no_character';
  static const bad_request_no_episode = 'bad_request.no_episode';
  static const bad_request = 'bad_request';
  static const reset_filter = 'reset_filter';
  static const character_origin = 'character_origin';
  static const character_location = 'character_location';
  static const episode = 'episode';
  static const air_date = 'air_date';

}
