// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes, avoid_renaming_method_parameters

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>?> load(String path, Locale locale) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> ru = {
  "characters": "Персонажи",
  "episodes": "Эпизоды",
  "locations": "Локации",
  "connect_timeout": {
    "failed_to_connect_to_the_system_please_check_internet_connection": "😥 не удалось подключиться к системе, пожалуйста проверьте подключение к интернету"
  },
  "network_connection": {
    "there_is_no_connection": "🌐 Нет подключения к интернету",
    "please_check_your_connection": "😶 Упс! У Вас Отсутствует подключение к интернету. Пожалуйста, проверьте Ваше соединение и повторите попытку."
  },
  "server_error": {
    "a_system_has_occurred": "😶 Упс! Произошла системная ошибка, пожалуйста попробуйте позже"
  },
  "oops_something_went_wrong_try_again": "😶 Упс! Что-то пошло не так, попробуйте ещё раз.",
  "there_is_nothing_here_yet": "🙂 здесь пока ничего нет",
  "repeat": "Повторить",
  "character_gender": {
    "gender_male": "Мужчина",
    "gender_female": "Женщина",
    "genderless": "Бесполый",
    "unknown": "Неизвестно"
  },
  "character_status": {
    "alive": "Жив",
    "dead": "Мёртв",
    "unknown": "Неизвестно"
  },
  "character_filter": {
    "name": "Имя",
    "species": "Вид",
    "type": "Тип",
    "gender": "Пол",
    "status": "Статус"
  },
  "search": "Поиск",
  "bad_request": {
    "no_character": "Персонаж не найден 🙂",
    "no_episode": "Эпизод не найден 🙂"
  },
  "reset_filter": "Сбросить фильтр",
  "character_origin": "Происхождение",
  "character_location": "Местоположение",
  "episode": "Эпизод",
  "air_date": "Дата выхода в эфир"
};
static const Map<String,dynamic> ky = {
  "characters": "Каармандар",
  "episodes": "Эпизоддор",
  "locations": "Жерлер",
  "connect_timeout": {
    "failed_to_connect_to_the_system_please_check_internet_connection": "😥 не удалось подключиться к системе, пожалуйста проверьте подключение к интернету"
  },
  "network_connection": {
    "there_is_no_connection": "🌐 Нет подключения к интернету",
    "please_check_your_connection": "😶 Упс! У Вас Отсутствует подключение к интернету. Пожалуйста, проверьте Ваше соединение и повторите попытку."
  },
  "server_error": {
    "a_system_has_occurred": "😶 Упс! Произошла системная ошибка, пожалуйста попробуйте позже"
  },
  "oops_something_went_wrong_try_again": "😶 Ой! Бир нерсе туура эмес болуп кетти, дагы бир жолу аракет кылып көрүңүз.",
  "there_is_nothing_here_yet": "🙂 бул жерде азырынча эч нерсе жок",
  "repeat": "Кайталоо",
  "character_gender": {
    "gender_male": "Эркек",
    "gender_female": "Аял",
    "genderless": "Жыныссыз",
    "unknown": "Белгисиз"
  },
  "character_status": {
    "alive": "Тирүү",
    "dead": "Өлдү",
    "unknown": "Белгисиз"
  },
  "character_filter": {
    "name": "Аты",
    "species": "Түрлөрү",
    "type": "Түрү",
    "gender": "Жынысы",
    "status": "Статусу"
  },
  "search": "Издөө",
  "bad_request": {
    "no_character": "Каарман табылган жок 🙂",
    "no_episode": "Эпизод табылган жок 🙂"
  },
  "reset_filter": "СКайтаруу чыпкасы",
  "character_origin": "Келип чыгышы",
  "character_location": "Жайгашкан жери",
  "episode": "Эпизод",
  "air_date": "Эфир чыккан"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"ru": ru, "ky": ky};
}
