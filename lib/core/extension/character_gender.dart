import 'package:rick_and_morty_app/core/enums/enums.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

extension CharacterGenderExt on CharacterGender {
  String checkGender() {
    return switch (this) {
      CharacterGender.male => LocaleKeys.character_gender_gender_male.tr(),
      CharacterGender.female => LocaleKeys.character_gender_gender_female.tr(),
      CharacterGender.genderless => LocaleKeys.character_gender_genderless.tr(),
      CharacterGender.unknown => LocaleKeys.character_gender_unknown.tr(),
    };
  }
}
