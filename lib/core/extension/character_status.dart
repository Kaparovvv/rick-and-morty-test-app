import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/enums/enums.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

extension CharacterStatusExt on CharacterStatus {
  Text checkStatus() {
    return switch (this) {
      CharacterStatus.alive => Text(
          LocaleKeys.character_status_alive.tr(),
          style: const TextStyle(color: AppColors.kGreen),
        ),
      CharacterStatus.dead => Text(
          LocaleKeys.character_status_dead.tr(),
          style: const TextStyle(color: AppColors.kRed),
        ),
      CharacterStatus.unknown => Text(
          LocaleKeys.character_status_unknown.tr(),
          style: const TextStyle(color: AppColors.kBrown),
        ),
    };
  }
}
