enum CharacterGender {
  male('Male', 'Мужской'),
  female('Female', 'Женский'),
  genderless('Genderless', 'Безполый'),
  unknown('unknown', 'Неизвестно');

  const CharacterGender(this.key, this.name);

  final String key;

  final String name;
}
