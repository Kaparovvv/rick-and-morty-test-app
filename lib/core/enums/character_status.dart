enum CharacterStatus {
  alive('Alive', 'Жив'),
  dead('Dead', 'Мертв'),
  unknown('unknown', 'Неизвестно');

  const CharacterStatus(this.key, this.name);

  final String key;
  final String name;
}
