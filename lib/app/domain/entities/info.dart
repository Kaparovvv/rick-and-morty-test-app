import 'package:rick_and_morty_app/core/packages/packages.dart';

class InfoEntity extends Equatable {
  final int count;
  final int pages;
  final String? next;
  final dynamic prev;

  const InfoEntity({
    required this.pages,
    required this.count,
    required this.next,
    required this.prev,
  });

  @override
  List<Object?> get props => [
        count,
        pages,
        next,
        prev,
      ];
}
