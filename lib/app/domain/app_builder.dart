import 'package:rick_and_morty_app/app/domain/app_runner.dart';

abstract class AppRunner {
  Future<void> preloadData();

  Future<void> run(AppBuilder appBuilder);
}
