import 'package:rick_and_morty_app/app/domain/entities/entities.dart';

class InfoModel extends InfoEntity {
  const InfoModel({
    required super.pages,
    required super.count,
    required super.next,
    required super.prev,
  });

  InfoModel copyWith({
    int? count,
    int? pages,
    String? next,
    dynamic prev,
  }) =>
      InfoModel(
        count: count ?? this.count,
        pages: pages ?? this.pages,
        next: next ?? this.next,
        prev: prev ?? this.prev,
      );

  factory InfoModel.fromJson(Map<String, dynamic> json) => InfoModel(
        count: json["count"],
        pages: json["pages"],
        next: json["next"],
        prev: json["prev"],
      );
}
