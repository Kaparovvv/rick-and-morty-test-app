import 'package:rick_and_morty_app/app/data/failure/failure.dart';
import 'package:rick_and_morty_app/core/enums/enums.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

extension FailureOfMain on DataStatus {
  Failure getFailureOfMain() {
    switch (this) {
      case DataStatus.noContent:
        return Failure(
          message: LocaleKeys.there_is_nothing_here_yet.tr(),
        );
      case DataStatus.badRequest:
        return Failure(
          message: LocaleKeys.oops_something_went_wrong_try_again.tr(),
          status: DataStatus.badRequest,
        );

      ///Здесь решено вывести дефолтную ошибку, так как авторизации и регистрации в приложении нет
      case DataStatus.forbidden:
        return Failure(
          message: LocaleKeys.oops_something_went_wrong_try_again.tr(),
        );

      ///Здесь решено вывести дефолтную ошибку, так как авторизации и регистрации в приложении нет

      case DataStatus.unauthorized:
        return Failure(
            message: LocaleKeys.oops_something_went_wrong_try_again.tr());
      case DataStatus.notFound:
        return Failure(
          message: LocaleKeys.server_error_a_system_has_occurred.tr(),
        );
      case DataStatus.internalServerError:
        return Failure(
          message: LocaleKeys.server_error_a_system_has_occurred.tr(),
        );
      case DataStatus.connectTimeout:
        return Failure(
          message: LocaleKeys
              .connect_timeout_failed_to_connect_to_the_system_please_check_internet_connection
              .tr(),
        );
      case DataStatus.cancel:
        return Failure(
          message: LocaleKeys.server_error_a_system_has_occurred.tr(),
        );
      case DataStatus.receiveTimeout:
        return Failure(
          message: LocaleKeys.server_error_a_system_has_occurred.tr(),
        );
      case DataStatus.sendTimeOut:
        return Failure(
          message: LocaleKeys
              .connect_timeout_failed_to_connect_to_the_system_please_check_internet_connection
              .tr(),
        );
      case DataStatus.cacheError:
        return Failure(
          message: LocaleKeys.oops_something_went_wrong_try_again.tr(),
        );
      case DataStatus.noInternetConnect:
        return Failure(
          message:
              LocaleKeys.network_connection_please_check_your_connection.tr(),
        );
      case DataStatus.defaultStatus:
        return Failure(
          message: LocaleKeys.oops_something_went_wrong_try_again.tr(),
        );
      default:
        return Failure(
          message: LocaleKeys.oops_something_went_wrong_try_again.tr(),
        );
    }
  }
}
