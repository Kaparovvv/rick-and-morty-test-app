import 'package:dio/dio.dart';
import 'package:rick_and_morty_app/app/data/error_handler/failure_of_main.dart';
import 'package:rick_and_morty_app/app/data/failure/failure.dart';
import 'package:rick_and_morty_app/core/enums/enums.dart';

class ErrorHandler implements Exception {
  late Failure failure;

  ErrorHandler.handle(dynamic error) {
    failure = _mainHandleError(error);
  }
}

Failure _mainHandleError(DioException error) {
  switch (error.type) {
    case DioExceptionType.connectionTimeout:
      return DataStatus.connectTimeout.getFailureOfMain();
    case DioExceptionType.sendTimeout:
      return DataStatus.sendTimeOut.getFailureOfMain();
    case DioExceptionType.receiveTimeout:
      return DataStatus.receiveTimeout.getFailureOfMain();
    case DioExceptionType.badResponse:
      final statusCode = error.response?.statusCode;
      if (statusCode != null) {
        if (statusCode == 401) {
          return DataStatus.unauthorized.getFailureOfMain();
        } else if (statusCode == 403) {
          return DataStatus.forbidden.getFailureOfMain();
        } else if (statusCode >= 500) {
          return DataStatus.internalServerError.getFailureOfMain();
        } else {
          return DataStatus.badRequest.getFailureOfMain();
        }
      } else {
        return DataStatus.badRequest.getFailureOfMain();
      }

    case DioExceptionType.cancel:
      return DataStatus.cancel.getFailureOfMain();
    case DioExceptionType.connectionError:
      return DataStatus.noInternetConnect.getFailureOfMain();
    default:
      return DataStatus.defaultStatus.getFailureOfMain();
  }
}
