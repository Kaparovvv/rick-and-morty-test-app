import 'package:rick_and_morty_app/core/enums/enums.dart';

class Failure {
  final String message;
  final DataStatus? status;
  Failure({
    required this.message,
    this.status,
  });
}
