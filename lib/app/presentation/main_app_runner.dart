import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rick_and_morty_app/app/domain/app_builder.dart';
import 'package:rick_and_morty_app/app/domain/app_runner.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/core/services/services.dart';
import 'package:rick_and_morty_app/di/di.dart';
import 'package:rick_and_morty_app/generated/codegen_loader.g.dart';

class MainAppRunner implements AppRunner {
  final String env;

  const MainAppRunner(this.env);

  @override
  Future<void> preloadData() async {
    WidgetsFlutterBinding.ensureInitialized();
    await EasyLocalization.ensureInitialized();
    configureDependencies(env);
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    Bloc.observer = const SimpleBlocObserver();
  }

  @override
  Future<void> run(AppBuilder appBuilder) async {
    await preloadData();
    runApp(
      EasyLocalization(
        supportedLocales: const [Locale('ru'), Locale('ky')],
        path: 'assets/translations',
        fallbackLocale: const Locale('ru'),
        assetLoader: const CodegenLoader(),
        child: appBuilder.buildApp(),
      ),
    );
  }
}
