import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/domain/app_runner.dart';
import 'package:rick_and_morty_app/application.dart';

class MainAppBuilder implements AppBuilder {
  @override
  Widget buildApp() {
    return const RickAndMortyApp();
  }
}
