import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/app/presentation/widgets/widgets.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/gen/assets.gen.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

class SearchFieldWidget extends StatelessWidget {
  const SearchFieldWidget({
    super.key,
    required this.controller,
    required this.focusNode,
    required this.hasReset,
    required this.onReset,
    required this.onChanged,
  });

  final TextEditingController controller;
  final FocusNode focusNode;
  final bool hasReset;
  final VoidCallback onReset;
  final void Function(String) onChanged;

  @override
  Widget build(BuildContext context) {
    return CustomTextFieldWidget(
      controller: controller,
      hinText: LocaleKeys.search.tr(),
      focusNode: focusNode,
      suffixIcon: hasReset
          ? IconButton(
              onPressed: onReset,
              icon: Assets.icons.icDeleteCircle.svg(
                colorFilter: ColorFilter.mode(
                  AppColors.kGrey500,
                  BlendMode.srcIn,
                ),
              ),
            )
          : null,
      prefixIcon: IconButton(
        onPressed: null,
        icon: Assets.icons.icSearch.svg(
          colorFilter: ColorFilter.mode(
            AppColors.kGrey500,
            BlendMode.srcIn,
          ),
        ),
      ),
      onTapOutside: (_) => focusNode.unfocus(),
      onChanged: onChanged,
    );
  }
}
