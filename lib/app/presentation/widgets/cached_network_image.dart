import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/commons/commons.dart';
import 'package:rick_and_morty_app/core/extension/extension.dart';
import 'package:rick_and_morty_app/core/gen/assets.gen.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';

class CachedNetworkImageWidget extends StatelessWidget {
  const CachedNetworkImageWidget({
    super.key,
    required this.width,
    required this.height,
    required this.imageUrl,
    this.radius,
    this.isRadius = true,
    this.shape,
    this.fit,
    this.errorWidget,
    this.errorListener,
  });

  final bool isRadius;
  final String? imageUrl;
  final double width;
  final double height;
  final BorderRadius? radius;
  final BoxShape? shape;
  final BoxFit? fit;
  final Widget? errorWidget;
  final void Function(Object)? errorListener;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      width: width,
      height: height,
      imageUrl: imageUrl ?? ImageHelper.defImageUrl,
      imageBuilder: (context, imageProvider) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: isRadius ? radius : null,
            shape: shape ?? BoxShape.rectangle,
            image: DecorationImage(
              image: imageProvider,
              fit: fit ?? BoxFit.cover,
            ),
          ),
        );
      },
      placeholder: (context, url) => Shimmer.fromColors(
        baseColor: AppColors.kGrey300,
        highlightColor: AppColors.kGrey500,
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
            color: AppColors.kBlack,
            borderRadius: radius,
            shape: shape ?? BoxShape.rectangle,
          ),
        ),
      ),
      errorListener: errorListener,
      errorWidget: (context, url, error) {
        return errorWidget ??
            Container(
              width: context.width * 0.2133,
              height: context.height * 0.0985,
              decoration: BoxDecoration(
                borderRadius: radius,
                shape: shape ?? BoxShape.rectangle,
              ),
              child: Assets.images.defaultImage.image(
                width: context.width * 0.2133,
                height: context.height * 0.0985,
              ),
            );
      },
    );
  }
}
