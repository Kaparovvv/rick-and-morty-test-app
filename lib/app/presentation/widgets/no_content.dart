import 'package:flutter/material.dart';

import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

class NotContentWidget extends StatelessWidget {
  const NotContentWidget({super.key, this.message, this.onTap});

  final String? message;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              message ?? LocaleKeys.there_is_nothing_here_yet.tr(),
              textAlign: TextAlign.center,
            ),
            if (onTap != null) ...[
              const Gap(16),
              ElevatedButton(
                onPressed: onTap,
                child: Text(
                  LocaleKeys.repeat.tr(),
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}
