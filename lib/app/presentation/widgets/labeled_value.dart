import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';

class LabeledValueWidget extends StatelessWidget {
  const LabeledValueWidget({
    super.key,
    required this.label,
    required this.value,
  });

  final String label;
  final String value;

  @override
  Widget build(BuildContext context) {
    final t = Theme.of(context);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Text(
            '$label:',
            style: t.textTheme.titleMedium?.copyWith(color: AppColors.kGrey600),
          ),
        ),
        const Gap(24),
        Flexible(
          child: Text(
            value,
            textAlign: TextAlign.right,
            style: t.textTheme.titleMedium,
          ),
        ),
      ],
    );
  }
}
