import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

class ErrorMessageWidget extends StatelessWidget {
  const ErrorMessageWidget({
    super.key,
    required this.message,
    this.label,
    required this.onTap,
  });

  final String message;
  final String? label;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    final t = Theme.of(context);
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              message,
              textAlign: TextAlign.center,
            ),
            const Gap(16),
            ElevatedButton(
              onPressed: onTap,
              child: Text(
                label ?? LocaleKeys.repeat.tr(),
                style:
                    t.textTheme.titleMedium?.copyWith(color: AppColors.kWhite),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
