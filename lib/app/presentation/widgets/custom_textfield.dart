import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/config/theme/theme.dart';

class CustomTextFieldWidget extends StatelessWidget {
  const CustomTextFieldWidget({
    super.key,
     this.controller,
    this.keyboardType,
    this.maxLines,
    this.hinText,
    this.validator,
    this.textCapitalization,
    this.focusNode,
    this.maxLength,
    this.onTap,
    this.onChanged,
    this.onFieldSubmitted,
    this.onTapOutside,
    this.errorText,
    this.fillColor,
    this.radius,
    this.contentPadding,
    this.cursorHeight,
    this.hintStyle,
    this.icon,
    this.autoFocus,
    this.prefixIcon,
    this.suffixIcon,
    this.textInputAction,
    this.prefix,
    this.readOnly,
    this.obscureText,
    this.autovalidateMode,
  });

  final FormFieldValidator<String>? validator;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final int? maxLines;
  final String? hinText;
  final TextCapitalization? textCapitalization;
  final FocusNode? focusNode;
  final int? maxLength;
  final Function()? onTap;
  final void Function(String)? onChanged;
  final Function(String)? onFieldSubmitted;
  final void Function(PointerDownEvent)? onTapOutside;
  final String? errorText;
  final Color? fillColor;
  final double? radius;
  final TextStyle? hintStyle;
  final EdgeInsetsGeometry? contentPadding;
  final double? cursorHeight;
  final Widget? icon;
  final bool? autoFocus;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final Widget? prefix;
  final bool? obscureText;
  final bool? readOnly;
  final TextInputAction? textInputAction;
  final AutovalidateMode? autovalidateMode;

  @override
  Widget build(BuildContext context) {
    final t = Theme.of(context);
    return TextFormField(
      readOnly: readOnly ?? false,
      autofocus: autoFocus ?? false,
      focusNode: focusNode ?? FocusNode(),
      controller: controller,
      maxLines: maxLines ?? 1,
      maxLength: maxLength,
      cursorHeight: cursorHeight,
      cursorRadius: const Radius.circular(5),
      autovalidateMode: autovalidateMode,
      keyboardType: keyboardType ?? TextInputType.text,
      textInputAction: textInputAction ?? TextInputAction.next,
      textCapitalization: textCapitalization ?? TextCapitalization.none,
      obscureText: obscureText ?? false,
      decoration: InputDecoration(
        icon: icon,
        contentPadding: contentPadding,
        errorMaxLines: 4,
        filled: true,
        fillColor: fillColor ?? AppColors.kBgTextField,
        hintText: hinText,
        isDense: true,
        prefixIconConstraints: const BoxConstraints(minWidth: 0, minHeight: 0),
        errorText: errorText,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        suffixIconConstraints: const BoxConstraints(minWidth: 0, minHeight: 0),
        prefix: prefix,
        errorStyle: t.textTheme.bodyMedium?.copyWith(color: AppColors.kRed),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius ?? 16),
          gapPadding: 10,
          borderSide: BorderSide(color: AppColors.kGrey500),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius ?? 16),
          borderSide: BorderSide(color: AppColors.kGrey500),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius ?? 16),
          borderSide: BorderSide(color: AppColors.kGrey500),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius ?? 16),
          borderSide: const BorderSide(color: AppColors.kRed),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius ?? 16),
          borderSide: BorderSide(color: AppColors.kGrey500),
        ),
      ),
      onTap: onTap,
      validator: validator,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      onTapOutside: onTapOutside,
    );
  }
}
