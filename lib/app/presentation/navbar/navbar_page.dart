import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/config/router/app_router.gr.dart';
import 'package:rick_and_morty_app/core/gen/assets.gen.dart';
import 'package:rick_and_morty_app/core/packages/packages.dart';
import 'package:rick_and_morty_app/generated/locale_keys.g.dart';

@RoutePage()
class NavBarPage extends StatefulWidget {
  const NavBarPage({super.key});

  @override
  State<NavBarPage> createState() => _NavBarPageState();
}

class _NavBarPageState extends State<NavBarPage> {
  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter(
      routes: const [
        CharactersRoute(),
        EpisodesRoute(),
      ],
      builder: (context, child) {
        final tabsRouter = AutoTabsRouter.of(context);
        return Scaffold(
          body: child,
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: tabsRouter.activeIndex,
            onTap: (index) => tabsRouter.setActiveIndex(index),
            items: [
              BottomNavigationBarItem(
                label: LocaleKeys.characters.tr(),
                icon: Assets.icons.icCharacters.image(
                  width: 32,
                  height: 32,
                ),
              ),
              BottomNavigationBarItem(
                label: LocaleKeys.episodes.tr(),
                icon: Assets.icons.icEpisodes.image(
                  width: 32,
                  height: 32,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
